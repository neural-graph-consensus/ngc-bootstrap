import numpy as np
from typing import List
from neural_wrappers.graph import Node
from neural_wrappers.readers import CompoundDatasetReader, StaticBatchedDatasetReader, \
	RandomIndexDatasetReader, MergeBatchedDatasetReader
from cycleconcepts.nodes import RGB

from .carla_reader import getCarlaReader
from .slanic_reader import SlanicReader
from .dummy_reader import DummyReader

def mergeFn(x):
	Keys = x[0]["data"]
	MB = len(x)
	y = {}
	for k in Keys:
		batch = np.stack([x[i]["data"][k] for i in range(MB)])
		y[k] = batch
	return y, y

class DebugReader(CompoundDatasetReader):
	def __len__(self):
		return 10

# TODO: Perhaps get the nodes somehow else, not from model name itself? To simplify NGC vs. SL situation.
def getReader(args, nodes:List[Node], baseDir:str, randomize:bool):
	# For plotting purposes, always read RGB even if our graph doesn't use it.
	if not RGB in map(type, nodes):
		nodes = [*nodes, RGB()]

	Type = args.dataCfg["type"]
	if Type == "CARLA":
		reader = getCarlaReader(args, paths, nodes)
	elif Type == "Slanic":
		reader = SlanicReader(baseDir, nodes, args.modelCfg["hyperParameters"])
	elif Type == "Dummy":
		reader = DummyReader(baseDir, nodes, args.modelCfg["hyperParameters"])
	else:
		assert False, "Unknown reader: %s" % Type

	if randomize:
		reader = RandomIndexDatasetReader(reader, seed=args.trainCfg["seed"])
	if args.debug:
		print("[getReader] Debug mode. Setting data count to 10.")
		reader = DebugReader(reader)
	reader = MergeBatchedDatasetReader(reader, mergeFn)
	reader = StaticBatchedDatasetReader(reader, batchSize=args.trainCfg["batchSize"])
	return reader