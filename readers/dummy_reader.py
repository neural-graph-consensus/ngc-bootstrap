import os
import numpy as np
from typing import Optional, Dict, Callable, List, Tuple
from neural_wrappers.readers import DatasetReader
from functools import partial
from natsort import natsorted
from pathlib import Path

def getFn(dataset:DatasetReader, index, dim):
	path = dataset[dim][index]
	item = np.load(path)
	if path[-3 :] == "npz":
		item = item["arr_0"]
	return item.astype(np.float32)

def getDimTransforms():
	dimTransforms = {
		"rgb" : lambda x : x,
		"semantic" : lambda x : x,
		"depth" : lambda x : x,
		"yolo" : lambda x : x,
	}
	return dimTransforms

class DummyReader(DatasetReader):
	def __init__(self, baseDir, nodes, hyperParameters):
		print("[%s] Base dir: %s." % (str(self), baseDir))
		self.baseDir = baseDir
		self.hyperParameters = hyperParameters

		self.nodes = nodes
		self.nodeNames = self.getNodeNames()
		self.dataset, self.len = self.getPathsFromNodes()

		dataBuckets = {"data" : self.nodeNames}
		dimGetter = {k : partial(getFn, dim=k) for k in self.nodeNames}
		dimTransform = {"data" : {k : getDimTransforms()[k] for k in self.nodeNames}}
		super().__init__(dataBuckets=dataBuckets, dimGetter=dimGetter, dimTransform=dimTransform)

	def getPathsFromNodes(self):
		assert len(self.nodes) > 0
		nodes = [node.groundTruthKey for node in self.nodes]
		nodeDirs = {node : "%s/%s" % (self.baseDir, node) for node in nodes}
		paths = {node : list(Path(nodeDirs[node]).glob("*.npz")) for node in nodes}
		paths = {node : [os.path.realpath(os.path.abspath(str(x))) for x in paths[node]] for node in nodes}
		paths = {node : natsorted(paths[node]) for node in nodes}
		print("[%s::getPathsFromNodes] Getting paths from relevant nodes only." % str(self))
		numDataPerNodes = {node : len(paths[node]) for node in nodes}
		assert tuple(numDataPerNodes.values())[0] > 0, numDataPerNodes
		assert np.std(tuple(numDataPerNodes.values())) == 0, "Num data per node is not equal: %s" % numDataPerNodes
		print("[%s::getPathsFromNodes] Num data per node: %d" % (str(self), tuple(numDataPerNodes.values())[0]))
		return paths, tuple(numDataPerNodes.values())[0]

	def getNodeNames(self) -> str:
		nodeNames = [node.groundTruthKey for node in self.nodes]
		print("[%s::getNodeNames] Nodes: %s" % (str(self), nodeNames))
		return nodeNames

	def getDataset(self):
		return self.dataset

	def __len__(self):
		return self.len

	def __str__(self):
		return "DummyReader"