import os
import numpy as np
from typing import Optional, Dict, Callable, List, Tuple
from neural_wrappers.readers import DatasetReader
from neural_wrappers.readers.datasets.carla.normalizers import *
from functools import partial
from natsort import natsorted
from pathlib import Path

class CarlaPseudolabelsNpyReader(DatasetReader):
	def __init__(self, paths:str, dataBuckets:List[str], dimGetter:Dict[str, Callable], \
		dimTransform:Dict[str, Callable], desiredShape, hyperParameters:Optional[Dict]={}):
		dimTransform = {"data" : {k : partial(dimTransform[k], readerObj=self) for k in dimTransform}}
		super().__init__(dataBuckets={"data" : dataBuckets}, dimGetter=dimGetter, dimTransform=dimTransform)
		self.paths = paths
		self.dataset, self.len = self.getDatasetFiles()
		self.desiredShape = desiredShape
		self.hyperParameters = hyperParameters

	def getDatasetFiles(self):
		dataset = {}
		lens = []
		for dim in self.datasetFormat.dataBuckets["data"]:
			p = Path(self.paths[dim])
			# semantic_segmentation_0 vs semenatic_segmentation_gb_0
			dimFiles = list(p.glob("*%s_[0-9]*" % dim))
			dataset[dim] = natsorted([os.path.realpath(os.path.abspath(str(x))) for x in dimFiles])
			Len = len(dataset[dim])
			lens.append(Len)
			assert Len > 0, "Pseudolabels were not computed for dim '%s'. Path: %s" % (dim, self.paths[dim])
		assert np.std(lens) == 0, "Lens: %s" % lens
		return dataset, lens[0]

	def getDataset(self):
		return self.dataset

	def __len__(self):
		return self.len

def getFn(dataset:DatasetReader, index, dim):
	path = dataset[dim][index]
	item = np.load(path)
	if path[-3 :] == "npz":
		item = item["arr_0"]
	return item.astype(np.float32)

def getDimTransforms():
	dimTransforms = {
		"rgb" : rgbNorm,
		"semantic_segmentation" : semanticSegmentationNorm,
		"depth" : depthNorm,
		"optical_flow" : opticalFlowNorm,
		"halftone" : halftoneNorm,
		"edges" : edgesNorm
	}
	return dimTransforms

def getCarlaReader(args, paths, nodes):
	dimTransforms = getDimTransforms()
	# Convert from node-specific hyperparameters to one single dict
	hyperParameters = {}
	for X in args.modelCfg["hyperParameters"].values():
		for k, v in X.items():
			assert not k in hyperParameters
			hyperParameters[k] = v

	reader = CarlaPseudolabelsNpyReader(paths, dataBuckets=nodes, \
		dimGetter={k : partial(getFn, dim=k) for k in nodes}, \
		dimTransform={k : dimTransforms[k] for k in nodes}, \
		desiredShape=args.dataCfg["desiredShape"], hyperParameters=hyperParameters)
	return reader
