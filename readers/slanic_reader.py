import os
import numpy as np
from typing import Optional, Dict, Callable, List, Tuple
from neural_wrappers.readers import DatasetReader
from functools import partial
from natsort import natsorted
from pathlib import Path
from media_processing_lib.image import imgResize

from .dummy_reader import DummyReader

def getFn(dataset:DatasetReader, index, dim):
	path = dataset[dim][index]
	item = np.load(path)
	if path[-3 :] == "npz":
		item = item["arr_0"]
	return item.astype(np.float32)

def default(x : np.ndarray) -> np.ndarray:
	return np.float32(x)

def depthNorm(x : np.ndarray) -> np.ndarray:
	return np.expand_dims(x, axis=-1)

def semanticNorm(x):
	assert x.min() >= 0 and x.max() <= 1
	return x

def edgesNorm(x):
	return np.expand_dims(x, axis=-1)

def opticalFlowNorm(x:np.ndarray, mode:str) -> np.ndarray:
	def opticalFlowMagnitude(x):
		x = np.hypot(x[..., 0], x[..., 1])
		# norm :: [0 : sqrt(2)] => [0 : 1]
		# x = x / np.sqrt(2)
		return np.expand_dims(x, axis=-1)

	# x :: [-1 : 1]
	if mode == "xy":
		return x
	elif mode == "magnitude":
		return opticalFlowMagnitude(x)
	elif mode == "xy_plus_magnitude":
		return np.concatenate([x, opticalFlowMagnitude(x)], axis=-1)
	assert False

def getDimTransforms(readerObj, dim):
	if dim in ("rgb", "hsv", "halftone1", "semanticgb1", "normals1"):
		return default
	elif dim in ("semantic1", "semanticsegprop1"):
		return semanticNorm
	elif dim in ("edges1", "edges2"):
		return edgesNorm
	elif dim in ("depth1", "depth2"):
		return depthNorm
	elif dim =="opticalflow1":
		return partial(opticalFlowNorm, mode=readerObj.hyperParameters["opticalflow1"]["opticalFlowMode"])
	else:
		print("Unknown dim: %s" % dim)
		assert False

class SlanicReader(DummyReader):
	def __init__(self, baseDir, nodes, hyperParameters):
		print("[%s] Base dir: %s." % (str(self), baseDir))
		self.baseDir = baseDir
		self.hyperParameters = hyperParameters

		self.nodes = nodes
		self.nodeNames = self.getNodeNames()
		self.dataset, self.len = self.getPathsFromNodes()

		dataBuckets = {"data" : self.nodeNames}
		dimGetter = {k : partial(getFn, dim=k) for k in self.nodeNames}
		dimTransform = {"data" : {k : getDimTransforms(readerObj=self, dim=k) for k in self.nodeNames}}
		DatasetReader.__init__(self, dataBuckets=dataBuckets, dimGetter=dimGetter, dimTransform=dimTransform)

	def __str__(self) -> str:
		return "SlanicReader"