import yaml
import numpy as np
import sys
from argparse import ArgumentParser
from neural_wrappers.utilities import changeDirectory

from train import trainSingleLinks
from test import testAndExportResults, testSingleLink, testSingleLinks
from test_and_build_greedy import testAndBuildGreedy
from test_and_build_exhaustive import testAndBuildExhaustive, getMetricIxAndDirection, getDfSL
from export_pseudolabels import exportPseudolabels
from make_working_directories import makeWorkingDirectories
from export_video import exportVideo
# from find_best_threshold import findBestThreshold

def getParser():
	parser = ArgumentParser()
	parser.add_argument("type")
	parser.add_argument("--modelCfg", required=True)
	parser.add_argument("--dataCfg", required=True)
	parser.add_argument("--trainCfg", required=True)
	parser.add_argument("--dir", required=True)
	parser.add_argument("--iteration", type=int, required=True)
	parser.add_argument("--seed", type=int, default=42)
	parser.add_argument("--whichSingleLink", default="all")
	# If this flag is set, we don't check for --dir existing already and we set numEpochs to 5
	# nw_module's iterations per epoch must be set to smaller number too or find a way to tell __len__ of reader to
	#  be small as well.
	parser.add_argument("--debug", type=int, default=0)
	parser.add_argument("--exist_ok", type=int, default=0)
	return parser

def getArgs(parser=None):
	if parser is None:
		parser = getParser()
	args = parser.parse_args()
	args.modelCfg = yaml.load(open(args.modelCfg, "r"), Loader=yaml.SafeLoader)
	args.dataCfg = yaml.load(open(args.dataCfg, "r"), Loader=yaml.SafeLoader)
	args.trainCfg = yaml.load(open(args.trainCfg, "r"), Loader=yaml.SafeLoader)
	if args.debug == 1 or args.type in ("export_video", "test", "export_pseudolabels"):
		args.exist_ok = 1
	args.exist_ok = bool(args.exist_ok)
	args = makeWorkingDirectories(args)
	return args

def main():
	args = getArgs()

	# Do full process for current iteration. Basically, train the single links and dual hop links, test on test set and
	#  export pseudolabels for current CFG (depth, semantic, etc...).
	if args.type == "full":
		trainSingleLinks(args)
		testAndExportResults(args, fileName="results.txt")
		exportPseudolabels(args)
	elif args.type == "train_single_links":
		trainSingleLinks(args)
	elif args.type == "test_single_link":
		assert args.whichSingleLink != "all"
		res = testSingleLink(args)
		print(res)
	elif args.type == "test_all_single_links":
		assert args.whichSingleLink == "all"
		slResults = testSingleLinks(args)
		unorderedGraphEdges = np.array(args.modelCfg["graph"]["edges"])
		metricNames = getMetricIxAndDirection(args, slResults)[2]
		dfSL = getDfSL(unorderedGraphEdges, slResults, metricNames)
		print(dfSL)
	elif args.type == "test":
		testAndExportResults(args, fileName="results.txt")
	elif args.type == "test_and_build_greedy":
		testAndBuildGreedy(args, fileName="results_greedy.txt")
	elif args.type == "test_and_build_exhaustive":
		testAndBuildExhaustive(args, fileName="results_exhaustive.txt")
	elif args.type == "export_pseudolabels":
		exportPseudolabels(args)
	elif args.type == "export_video":
		exportVideo(args, videoName="result.mp4")
	else:
		assert False

if __name__ == "__main__":
	main()