import os
import sys
import numpy as np
from overrides import overrides
from neural_wrappers.graph.node import MapNode
from neural_wrappers.metrics import MeanIoU, F1Score, Accuracy, MetricWrapper
from neural_wrappers.utilities import toCategorical
from cycleconcepts.nodes import RGB, Semantic

class Semantic1(Semantic):
	def __init__(self, semanticNumClasses:int, semanticUseAllMetrics:bool):
		self.numClasses = semanticNumClasses
		self.useAllMetrics = semanticUseAllMetrics
		MapNode.__init__(self, name="Semantic1", groundTruthKey="semantic1")

class SemanticSegProp1(Semantic):
	def __init__(self, semanticNumClasses:int, semanticUseAllMetrics:bool):
		self.numClasses = semanticNumClasses
		self.useAllMetrics = semanticUseAllMetrics
		MapNode.__init__(self, name="SemanticSegProp1", groundTruthKey="semanticsegprop1")

class SemanticGB1(RGB):
	def __init__(self):
		MapNode.__init__(self, name="SemanticGB1", groundTruthKey="semanticgb1")

	def getMetrics(self):
		return {
			"SemanticGB (L1 pixel)" : SemanticGB1.RGBMetricL1Pixel,
			"SemanticGB (L2)" : SemanticGB1.RGBMetricL2
		}