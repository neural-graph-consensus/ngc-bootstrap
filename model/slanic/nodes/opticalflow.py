from neural_wrappers.graph.node import MapNode
from functools import partial
from cycleconcepts.models import EncoderMap2Map, DecoderMap2Map

class OpticalFlow(MapNode):
	def __init__(self, opticalFlowMode):
		assert opticalFlowMode in ("magnitude", "xy_plus_magnitude")
		self.NC = {
			"magnitude" : 1,
			"xy_plus_magnitude" : 3
		}[opticalFlowMode]
		self.opticalFlowMode = opticalFlowMode
		hyperParameters = {"opticalFlowMode" : opticalFlowMode}
		super().__init__(name="OpticalFlow", groundTruthKey="optical_flow", hyperParameters=hyperParameters)

	def getEncoder(self, outputNodeType=None):
		return partial(EncoderMap2Map, dIn=self.NC)()

	def getDecoder(self, inputNodeType=None):
		return partial(DecoderMap2Map, dOut=self.NC)()

	def getMetrics(self):
		metrics = {}
		return metrics

	def getCriterion(self):
		return OpticalFlow.lossFn

	def lossFn(y, t):
		L = ((y - t)**2).mean()
		return L

class OpticalFlow1(OpticalFlow):
	def __init__(self, opticalFlowMode):
		assert opticalFlowMode in ("magnitude", "xy_plus_magnitude")
		self.NC = {
			"magnitude" : 1,
			"xy_plus_magnitude" : 3
		}[opticalFlowMode]
		self.opticalFlowMode = opticalFlowMode
		hyperParameters = {"opticalFlowMode" : opticalFlowMode}
		MapNode.__init__(self, name="OpticalFlow1", groundTruthKey="opticalflow1", hyperParameters=hyperParameters)
