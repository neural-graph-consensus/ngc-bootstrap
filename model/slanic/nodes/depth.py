import os
import sys
from cycleconcepts.nodes import Depth
from neural_wrappers.graph.node import MapNode

class Depth1(Depth):
	def __init__(self, maxDepthMeters):
		self.maxDepthMeters = maxDepthMeters
		hyperParameters = {"maxDepthMeters" : maxDepthMeters}
		MapNode.__init__(self, name="Depth1", groundTruthKey="depth1", hyperParameters=hyperParameters)

class Depth2(Depth):
	def __init__(self, maxDepthMeters):
		self.maxDepthMeters = maxDepthMeters
		hyperParameters = {"maxDepthMeters" : maxDepthMeters}
		MapNode.__init__(self, name="Depth2", groundTruthKey="depth2", hyperParameters=hyperParameters)
