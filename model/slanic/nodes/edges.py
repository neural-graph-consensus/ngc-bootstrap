from neural_wrappers.graph.node import MapNode
from functools import partial
from cycleconcepts.models import EncoderMap2Map, DecoderMap2Map

class Edges1(MapNode):
	def __init__(self):
		super().__init__(name="Edges1", groundTruthKey="edges1")

	def getEncoder(self, outputNodeType=None):
		return partial(EncoderMap2Map, dIn=1)()

	def getDecoder(self, inputNodeType=None):
		return partial(DecoderMap2Map, dOut=1)()

	def getMetrics(self):
		metrics = {}
		return metrics

	def getCriterion(self):
		return Edges.lossFn

	def lossFn(y, t):
		L = ((y - t)**2).mean()
		return L

class Edges2(Edges1):
	def __init__(self):
		MapNode.__init__(self, name="Edges2", groundTruthKey="edges2")
