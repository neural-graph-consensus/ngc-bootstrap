from cycleconcepts.nodes import Normal
from neural_wrappers.graph.node import MapNode

class Normals1(Normal):
	def __init__(self):
		MapNode.__init__(self, name="Normals1", groundTruthKey="normals1")

	def getMetrics(self):
		return {
			"Normal (deg)" : Normal.degreeMetric,
		}