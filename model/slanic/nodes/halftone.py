import os
import sys
from cycleconcepts.nodes import Halftone
from neural_wrappers.graph.node import MapNode

class Halftone1(Halftone):
	def __init__(self):
		MapNode.__init__(self, name="Halftone1", groundTruthKey="halftone1")
