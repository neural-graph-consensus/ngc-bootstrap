from cycleconcepts.nodes import RGB
from neural_wrappers.graph.node import MapNode

class HSV(RGB):
	def __init__(self):
		MapNode.__init__(self, name="HSV", groundTruthKey="hsv")

	def getMetrics(self):
		return {
			"HSV (L1 pixel)" : SemanticGB.RGBMetricL1Pixel,
			"HSV (L2)" : SemanticGB.RGBMetricL2
		}