from neural_wrappers.graph import Edge
from ..nodes import SemanticGB1, Semantic1
from cycleconcepts.models import SingleLinkGraph

def semanticgb1_semantic1(hyperParameters) -> SingleLinkGraph:
	semanticGBNode = SemanticGB1()
	semanticNode = Semantic1(semanticNumClasses=hyperParameters["semanticNumClasses"], \
		semanticUseAllMetrics=hyperParameters["semanticUseAllMetrics"])
	return SingleLinkGraph([
		Edge(semanticGBNode, semanticNode, blockGradients=False)
	])
