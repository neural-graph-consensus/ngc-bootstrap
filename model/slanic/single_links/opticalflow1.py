from neural_wrappers.graph import Edge
from ..nodes import OpticalFlow1, Semantic1
from cycleconcepts.models import SingleLinkGraph

def opticalflow1_semantic1(hyperParameters) -> SingleLinkGraph:
	opticalFlowNode = OpticalFlow1(opticalFlowMode=hyperParameters["opticalFlowMode"])
	semanticNode = Semantic1(semanticNumClasses=hyperParameters["semanticNumClasses"], \
		semanticUseAllMetrics=hyperParameters["semanticUseAllMetrics"])

	return SingleLinkGraph([
		Edge(opticalFlowNode, semanticNode)
	])