from neural_wrappers.graph import Edge
from ..nodes import Edges2, Semantic1
from cycleconcepts.models import SingleLinkGraph

def edges2_semantic1(hyperParameters) -> SingleLinkGraph:
	edgesNode = Edges2()
	semanticNode = Semantic1(semanticNumClasses=hyperParameters["semanticNumClasses"], \
		semanticUseAllMetrics=hyperParameters["semanticUseAllMetrics"])

	return SingleLinkGraph([
		Edge(edgesNode, semanticNode)
	])
