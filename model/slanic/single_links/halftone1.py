from neural_wrappers.graph import Edge
from ..nodes import Halftone1, Semantic1
from cycleconcepts.models import SingleLinkGraph

def halftone1_semantic1(hyperParameters) -> SingleLinkGraph:
	halftoneNode = Halftone1()
	semanticNode = Semantic1(semanticNumClasses=hyperParameters["semanticNumClasses"], \
		semanticUseAllMetrics=hyperParameters["semanticUseAllMetrics"])
	return SingleLinkGraph([
		Edge(halftoneNode, semanticNode)
	])