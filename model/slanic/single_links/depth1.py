from neural_wrappers.graph import Edge
from ..nodes import Depth1, Semantic1
from cycleconcepts.models import SingleLinkGraph

def depth1_semantic1(hyperParameters) -> SingleLinkGraph:
	depthNode = Depth1(maxDepthMeters=hyperParameters["maxDepthMeters"])
	semanticNode = Semantic1(semanticNumClasses=hyperParameters["semanticNumClasses"], \
		semanticUseAllMetrics=hyperParameters["semanticUseAllMetrics"])
	return SingleLinkGraph([
		Edge(depthNode, semanticNode)
	])
