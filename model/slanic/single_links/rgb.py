from neural_wrappers.graph import Edge
from ..nodes import RGB, Semantic1
from cycleconcepts.models import SingleLinkGraph

def rgb_semantic1(hyperParameters) -> SingleLinkGraph:
	rgbNode = RGB()
	semanticNode = Semantic1(semanticNumClasses=hyperParameters["semanticNumClasses"], \
		semanticUseAllMetrics=hyperParameters["semanticUseAllMetrics"])
	return SingleLinkGraph([
		Edge(rgbNode, semanticNode, blockGradients=False)
	])