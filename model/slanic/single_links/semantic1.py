from neural_wrappers.graph import Edge
from cycleconcepts.nodes import *
from cycleconcepts.models import SingleLinkGraph

def semantic1_rgb(hyperParameters) -> SingleLinkGraph:
	semanticNode = Semantic1(semanticNumClasses=hyperParameters["semanticNumClasses"], \
		semanticUseAllMetrics=hyperParameters["semanticUseAllMetrics"])
	rgbNode = RGB()
	return SingleLinkGraph([
		Edge(semanticNode, rgbNode, blockGradients=False)
	])

def semantic1_wireframeRegression(hyperParameters) -> SingleLinkGraph:
	semanticNode = Semantic1(semanticNumClasses=hyperParameters["semanticNumClasses"], \
		semanticUseAllMetrics=hyperParameters["semanticUseAllMetrics"])
	wireframeRegressionNode = WireframeRegression()
	return SingleLinkGraph([
		Edge(semanticNode, wireframeRegressionNode, blockGradients=False)
	])

def semantic1_normal(hyperParameters) -> SingleLinkGraph:
	semanticNode = Semantic1(semanticNumClasses=hyperParameters["semanticNumClasses"], \
		semanticUseAllMetrics=hyperParameters["semanticUseAllMetrics"])
	normalNode = Normal()
	return SingleLinkGraph([
		Edge(semanticNode, normalNode, blockGradients=False)
	])

def semantic1_cameranormal(hyperParameters) -> SingleLinkGraph:
	semanticNode = Semantic1(semanticNumClasses=hyperParameters["semanticNumClasses"], \
		semanticUseAllMetrics=hyperParameters["semanticUseAllMetrics"])
	cameranormalNode = CameraNormal()
	return SingleLinkGraph([
		Edge(semanticNode, cameranormalNode, blockGradients=False)
	])

def semantic1_pose(hyperParameters) -> SingleLinkGraph:
	semanticNode = Semantic1(semanticNumClasses=hyperParameters["semanticNumClasses"], \
		semanticUseAllMetrics=hyperParameters["semanticUseAllMetrics"])
	poseNode = Pose(**hyperParameters)
	return SingleLinkGraph([
		Edge(semanticNode, poseNode, blockGradients=False)
	])

def semantic1_depth(hyperParameters) -> SingleLinkGraph:
	semanticNode = Semantic1(semanticNumClasses=hyperParameters["semanticNumClasses"], \
		semanticUseAllMetrics=hyperParameters["semanticUseAllMetrics"])
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	return SingleLinkGraph([
		Edge(semanticNode, depthNode, blockGradients=False)
	])

def semantic1_halftone(hyperParameters) -> SingleLinkGraph:
	semanticNode = Semantic1(semanticNumClasses=hyperParameters["semanticNumClasses"], \
		semanticUseAllMetrics=hyperParameters["semanticUseAllMetrics"])
	halftoneNode = Halftone()
	return SingleLinkGraph([
		Edge(semanticNode, halftoneNode, blockGradients=False)
	])
