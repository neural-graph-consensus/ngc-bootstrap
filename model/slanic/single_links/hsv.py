from neural_wrappers.graph import Edge
from ..nodes import HSV, Semantic1
from cycleconcepts.models import SingleLinkGraph

def hsv_semantic1(hyperParameters) -> SingleLinkGraph:
	hsvNode = HSV()
	semanticNode = Semantic1(semanticNumClasses=hyperParameters["semanticNumClasses"], \
		semanticUseAllMetrics=hyperParameters["semanticUseAllMetrics"])
	return SingleLinkGraph([
		Edge(hsvNode, semanticNode, blockGradients=False)
	])