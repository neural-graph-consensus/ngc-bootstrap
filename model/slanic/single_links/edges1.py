from neural_wrappers.graph import Edge
from ..nodes import Edges1, Semantic1
from cycleconcepts.models import SingleLinkGraph

def edges1_semantic1(hyperParameters) -> SingleLinkGraph:
	edgesNode = Edges1()
	semanticNode = Semantic1(semanticNumClasses=hyperParameters["semanticNumClasses"], \
		semanticUseAllMetrics=hyperParameters["semanticUseAllMetrics"])

	return SingleLinkGraph([
		Edge(edgesNode, semanticNode)
	])
