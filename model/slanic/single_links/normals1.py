from neural_wrappers.graph import Edge
from ..nodes import Normals1, Semantic1
from cycleconcepts.models import SingleLinkGraph

def normals1_semantic1(hyperParameters) -> SingleLinkGraph:
	normalsNode = Normals1()
	semanticNode = Semantic1(semanticNumClasses=hyperParameters["semanticNumClasses"], \
		semanticUseAllMetrics=hyperParameters["semanticUseAllMetrics"])

	return SingleLinkGraph([
		Edge(normalsNode, semanticNode)
	])
