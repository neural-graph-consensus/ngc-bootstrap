import sys
import os
import numpy as np
import torch as tr
import torch.nn as nn
from typing import Dict
from functools import partial

from neural_wrappers.graph import Graph, Node
from neural_wrappers.graph.node import MapNode
from neural_wrappers.pytorch import device

from .single_links import *
from ..utils import getModelHyperParameters

def getSlanicModel(modelName:str, modelCfg:Dict, dataCfg:Dict, trainCfg:Dict) -> nn.Module:	
	np.random.seed(trainCfg["seed"])
	tr.manual_seed(trainCfg["seed"])
	Node.lastNodeID = 0
	nodes = modelName.split("_")
	hyperParameters = getModelHyperParameters(nodes, modelCfg)
	model = globals()[modelName](hyperParameters).to(device)
	return model