import os
import sys
import torch as tr
import numpy as np
from functools import partial
from neural_wrappers.graph import Edge
from neural_wrappers.pytorch import device
from neural_wrappers.callbacks import RandomPlotEachEpoch
from .get_model import getModel
from plot_utils import plotFn

sys.path.append("%s/../../neural-graph-consensus" % (os.path.abspath(os.path.realpath(os.path.dirname(__file__)))))
from cycleconcepts.voting_algorithms import getVotingAlgorithm as getVotingAlgorithmOriginal
from cycleconcepts.models import GraphEnsemble
from cycleconcepts.nodes import RGB

def consensus1(self, x, prior):
	x.pop("GT")
	SL = x["HSV (ID: 0) -> Semantic1 (ID: 1)"]

	y = [*x.values()]
	y = tr.cat(y, dim=0)
	breakpoint()
	# maxPred = prediction.max(axis=-1)
	# numBins = len(prior)
	# Bin = np.int32(maxPred * numBins)
	# conf = prior[Bin]
	# confImage = np.stack([conf, conf, conf], axis=-1)
	# confImage = np.uint8(confImage * 255)
	# return confImage
	return y

def getVotingAlgorithm(votingAlgorithm:str):
	if votingAlgorithm in ("mean", "median"):
		return getVotingAlgorithmOriginal(votingAlgorithm)
	elif votingAlgorithm == "consensus1":
		return partial(consensus1, prior=np.load("experimental_scripts/testHist/normed_th.npy"))
	else:
		assert False

def getNGC(args):
	edgeModelNames = args.modelCfg["graph"]["edges"]
	models = [getModel(modelName, args.modelCfg, args.dataCfg, args.trainCfg) for modelName in edgeModelNames]
	outNode = models[0].edges[0].outputNode
	print("[getNGC] Model: NGC-%s." % outNode.groundTruthKey)

	ensembleEdges = []
	nodes = {}
	for model, modelName in zip(models, args.modelCfg["graph"]["edges"]):
		edge = model.edges[0]
		nodeA, nodeB = edge.inputNode, edge.outputNode
		# Small hack to reuse the same nodes from multiple edges.
		if not type(nodeA) in nodes:
			nodes[type(nodeA)] = nodeA
		if not type(nodeB) in nodes:
			nodes[type(nodeB)] = nodeB
		nodeA = nodes[type(nodeA)]
		nodeB = nodes[type(nodeB)]
		edge = Edge(nodeA, nodeB, forwardFn=edge.forwardFn, name=edge.name)
		ensembleEdges.append(edge)

	for edge, modelName in zip(ensembleEdges, edgeModelNames):
		edge.loadWeights("%s/%s/model_best.pkl" % (args.modelCfg["modelWorkingDir"], modelName), yolo=True)

	voteFunction = getVotingAlgorithm(args.modelCfg["graph"]["votingAlgorithm"])
	# ensembleEdges.append(voteEdge)

	graphModel = GraphEnsemble(args.modelCfg["modelWorkingDir"], ensembleEdges, voteFunction).to(device)
	callbacks = list(filter(lambda x : not isinstance(x, RandomPlotEachEpoch), models[0].getCallbacks()))
	callbacks.append(RandomPlotEachEpoch(partial(plotFn, model=model, dataset=args.dataCfg["type"], inNode=RGB())))
	graphModel.addCallbacks(callbacks)
	return graphModel