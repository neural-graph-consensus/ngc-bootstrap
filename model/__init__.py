from .get_model import getModel, getSingleLinks, getRGBSingleLink
from .get_ngc import getNGC
