from typing import Dict, List

def getModelHyperParameters(nodes:List[str], modelCfg:Dict) -> Dict:
	assert len(nodes) == 2
	hyperParameters = {}

	def combineDict(d1, d2):
		d = {k : d1[k] for k in d1}
		for k in d2:
			assert not k in d
			d[k] = d2[k]
		return d

	if nodes[0] in modelCfg["hyperParameters"]:
		hyperParameters = combineDict(hyperParameters, modelCfg["hyperParameters"][nodes[0]])
	if nodes[1] in modelCfg["hyperParameters"]:
		hyperParameters = combineDict(hyperParameters, modelCfg["hyperParameters"][nodes[1]])
	return hyperParameters