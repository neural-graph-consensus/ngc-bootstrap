import sys
import os
import numpy as np
import torch as tr
import torch.nn as nn
from typing import Dict
from functools import partial

from neural_wrappers.graph import Graph, Node, Edge
from neural_wrappers.graph.node import MapNode
from neural_wrappers.pytorch import device

sys.path.append("%s/../../neural-graph-consensus" % (os.path.abspath(os.path.realpath(os.path.dirname(__file__)))))
from cycleconcepts.nodes import RGB, Depth, Semantic as BaseSemantic
from cycleconcepts.models import SingleLinkGraph, EncoderMap2Map, DecoderMap2Map

from .utils import getModelHyperParameters

## Nodes

class Semantic(BaseSemantic):
	def __init__(self, semanticNumClasses):
		MapNode.__init__(self, name="Semantic", groundTruthKey="semantic")
		self.numClasses = semanticNumClasses

class YOLO(RGB):
	def __init__(self):
		MapNode.__init__(self, name="Yolo", groundTruthKey="yolo")

	def getEncoder(self, outputNodeType=None):
		return EncoderMap2Map(dIn=6)

	def getDecoder(self, inputNodeType=None):
		return DecoderMap2Map(dOut=6)

	def getMetrics(self):
		return {
			"YOLO (L2)" : Yolo.RGBMetricL2
		} 

## Edges

def rgb2depth(hyperParameters:Dict) -> SingleLinkGraph:
	rgbNode = RGB()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	return SingleLinkGraph([
		Edge(rgbNode, depthNode)
	])

def semantic2depth(hyperParameters:Dict) -> SingleLinkGraph:
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])

	return SingleLinkGraph([
		Edge(semanticNode, depthNode)
	])

def yolo2depth(hyperParameters:Dict) -> SingleLinkGraph:
	yoloNode = YOLO()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	return SingleLinkGraph([
		Edge(yoloNode, depthNode)
	])

def rgb2semantic(hyperParameters:Dict) -> SingleLinkGraph:
	rgbNode = RGB()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])

	return SingleLinkGraph([
		Edge(rgbNode, semanticNode)
	])

def getDummyModel(modelName:str, modelCfg:Dict, dataCfg:Dict, trainCfg:Dict) -> nn.Module:
	try:
		modelType = {
			"rgb2depth" : rgb2depth,
			"semantic2depth" : semantic2depth,
			"yolo2depth" : yolo2depth,
			"rgb2semantic" : rgb2semantic
		}[modelName]
	except Exception as e:
		print("[getDummyType] Model Type: %s" % modelName)
		assert False
	
	np.random.seed(trainCfg["seed"])
	tr.manual_seed(trainCfg["seed"])
	Node.lastNodeID = 0
	nodes = modelName.split("2")
	hyperParameters = getModelHyperParameters(nodes, modelCfg)
	model = modelType(hyperParameters).to(device)
	return model