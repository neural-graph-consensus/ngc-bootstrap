import os
import sys
import torch.nn as nn
import torch.optim as optim
from typing import Dict
from functools import partial
from neural_wrappers.schedulers import ReduceLRAndBacktrackOnPlateau
from neural_wrappers.callbacks import SaveHistory, SaveModels, PlotMetrics, RandomPlotEachEpoch

from plot_utils import plotFn
from .dummy_model import getDummyModel
from .slanic import getSlanicModel

def getModel(modelName:str, modelCfg:Dict, dataCfg:Dict, trainCfg:Dict) -> nn.Module:
	if dataCfg["type"] == "Dummy":
		model = getDummyModel(modelName, modelCfg, dataCfg, trainCfg)
	elif dataCfg["type"] == "Slanic":
		model = getSlanicModel(modelName, modelCfg, dataCfg, trainCfg)
	else:
		assert False, dataCfg["type"]

	model.name = modelName
	optimizerType = {
		"Adam" : optim.Adam
	}[trainCfg["optimizerType"]]

	model.setOptimizer(optimizerType, lr=trainCfg["learningRate"])
	if trainCfg["patience"] > 0:
		scheduler = ReduceLRAndBacktrackOnPlateau(model, "Loss", trainCfg["patience"], trainCfg["factor"])
		model.setOptimizerScheduler(scheduler)

	assert len(model.edges) == 1, model.edges
	edge = model.edges[0]
	edgeMetrics = [(str(edge), str(metric.name)) for metric in edge.getMetrics()]
	inNode = edge.getNodes()[0]

	allMetrics = [str(x.name) for x in model.getMetrics()] + edgeMetrics
	saveModels = [SaveModels("best", metric) for metric in allMetrics]
	model.addCallbacks([
		*saveModels,
		SaveModels("last", "Loss"),
		PlotMetrics(allMetrics),
		SaveHistory("history.txt"),
		RandomPlotEachEpoch(partial(plotFn, model=model, dataset=dataCfg["type"], inNode=inNode))
	])

	return model

def getSingleLinks(args):
	for modelName in args.modelCfg["graph"]["edges"]:
		print("[getSingleLinks] Model: %s." % modelName)
		if args.whichSingleLink != "all" and args.whichSingleLink != modelName:
			continue
		model = getModel(modelName, args.modelCfg, args.dataCfg, args.trainCfg)
		yield model

def getRGBSingleLink(args):
	return getModel(args.whichSingleLink, args.modelCfg, args.dataCfg, args.trainCfg)
