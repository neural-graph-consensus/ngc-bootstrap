import numpy as np
import pandas as pd
from tqdm import trange
from functools import partial
from typing import Optional, List
from neural_wrappers.graph import Graph
from neural_wrappers.utilities import changeDirectory
from neural_wrappers.callbacks import Callback
from cycleconcepts.models import SingleLinkGraph
from cycleconcepts.nodes import RGB

from readers import getReader
from model import getSingleLinks, getNGC
from test import testModel, testSingleLinks

# Instantiate a new model and get its metrics. Get the relevant metric index and direction for NGC processing
def getMetricIxAndDirection(args, slResults:np.ndarray):
	relevantMetric = args.modelCfg["relevantMetric"]
	modelName = args.modelCfg["graph"]["edges"][0]
	firstEdge = next(getSingleLinks(args)).edges[0]
	metrics = firstEdge.getMetrics()
	assert len(metrics) == slResults.shape[1]
	ix = metrics.index(relevantMetric)
	metric = metrics[ix]
	direction = metric.getDirection()
	metricNames = [str(x.name) for x in metrics]
	return ix, direction, metricNames

def getDfSL(unorderedGraphEdges, slResults, metricNames):
	rowsSL = []
	for edge, result in zip(unorderedGraphEdges, slResults):
		rowsSL.append([str(edge), *result])
	dfSL = pd.DataFrame(rowsSL, columns=["SL", *metricNames])
	return dfSL

def getDfNGC(ngcResults, metricNames):
	rowsNGC = []
	for i, result in enumerate(ngcResults):
		rowsNGC.append(["N=%d" % (i + 1), *result])
	dfNGC = pd.DataFrame(rowsNGC, columns=["NGC", *metricNames])
	return dfNGC

def printAndExportSummary(unorderedGraphEdges:np.ndarray, slResults:np.ndarray, ngcResults:np.ndarray, \
	picked:List[int], metricNames:List[str], relevantMetric:str, fileName:str):
	dfSL = getDfSL(unorderedGraphEdges, slResults, metricNames)
	dfNGC = getDfNGC(ngcResults, metricNames)

	Str = "Relevant metric: %s" % relevantMetric
	Str += "\nOrder: %s" % " > ".join(unorderedGraphEdges[picked])
	Str += "\n%s" % str(dfSL)
	Str += "\n%s" % str(dfNGC)
	print(Str)

	print("[testAndBuildExhaustive] Exporting to: %s" % fileName)
	f = open(fileName, "a")
	f.write(Str + "\n")
	f.close()

# Transfer b[ix] to a and return both modified copies.
def transfer(a, b, ix):
	_a = np.append(a.copy(), b[ix])
	_b = np.delete(b, ix)
	return _a, _b

# The idea is that the modelCfg's edges represent the fully path graph (xxx2target for all xxx input nodes)
# We'll benchmark each individual single link xxx2target, then, based on the validation performance, we build the NGC
#  link by link. In the end, we'll have a result for all possible graph CFGs (N=1,...,N=n)
def testAndBuildExhaustive(args, fileName:str):
	pd.set_option("display.max_colwidth", None)
	originalEdgeOrder = np.array(args.modelCfg["graph"]["edges"])
	slResults = testSingleLinks(args)
	metricIx, metricDirection, metricNames = getMetricIxAndDirection(args, slResults)
	fBest = lambda x : (np.argmin if metricDirection == "min" else np.argmax)(np.array(x)[:, metricIx])

	unorderedGraphEdges = np.array(args.modelCfg["graph"]["edges"])
	picked, left = np.array([], dtype=np.int32), np.arange(len(unorderedGraphEdges))

	# Remove best and move it to picked
	# try:
	# 	bestIx = np.where(np.array(tuple(map(lambda x : x.find("rgb") != -1, unorderedGraphEdges))))[0][0]
	# except Exception:
	# 	assert False
	bestIx = fBest(slResults)
	picked, left = transfer(picked, left, bestIx)
	ngcResults = [slResults[bestIx]]
	for i in trange(1, len(unorderedGraphEdges), desc="NGC"):
		thisResults = []
		# add 1 by 1 each edge and test the new NGC
		for j in range(len(left)):
			currentPicked, _ = transfer(picked, left, j)
			args.modelCfg["graph"]["edges"] = unorderedGraphEdges[currentPicked]
			model = getNGC(args)
			ngcResult = testModel(args, model)
			ngcMetrics = tuple(ngcResult["Vote"].values())
			ngcMetrics = np.array([x.mean() if isinstance(x, np.ndarray) else x for x in ngcMetrics])
			thisResults.append(ngcMetrics)
		# keep the best result and repeat for N+1 edges
		bestIx = fBest(thisResults)
		picked, left = transfer(picked, left, bestIx)
		ngcResults.append(thisResults[bestIx])
	assert len(left) == 0
	fileName = "%s/%s" % (args.dataCfg["testResultsDir"], fileName.split("/")[-1])
	printAndExportSummary(originalEdgeOrder, slResults, ngcResults, picked, metricNames, \
		args.modelCfg["relevantMetric"], fileName)

