## Algorithm
```
[Prerequisites]:
    - Get stationary data: data/rgb*
    - Get pseudolabels from Heaven: data_iter0/semantic*, data_iter0/depth*
    - Have 4 models: rgb2depth, rgb2semantic, depth2semantic, semantic2depth
    - Have a measure metric for: rgb2depth, rgb2semantic, (rgb2depth + rgb2semantic2depth), (rgb2semantic + rgb2depth+semantic)

Iter0:
[Depth]
    - Train rgb2depth on data/rgb* + data_iter0/depth*
    - Measure the metric on rgb2depth
    - Train semantic2depth on data_iter0/semantic* + data_iter0/depth*
    - Create graph (rgb2depth + semantic2depth)
    - Measure the metric of the graph
    - generate pseudolabels data_iter1/depth* using the graph

[Semantic]
    - Train rgb2semantic on data/rgb* + data_iter0/semanntic*
    - Measure the metric on rgb2semantic
    - Train depth2semantic on data_iter0/depth* + data_iter0/semantic*
    - Create graph (rgb2semantic + depth2semantic)
    - Measure the metric of the graph
    - generate pseudolabels data_iter1/semantic* using the graph
_____________________

Iter1:
[Depth]
    - Train rgb2depth on data/rgb* + data_iter1/depth*
    - Measure the metric on rgb2depth
    - ...

[Semantic]
    - Train rgb2semantic on data/rgb* + data_iter1/semantic*
    - Measure the metric on rgb2semantic
    ...
_____________________
Repeat until convergence
```

## Results

https://docs.google.com/document/d/1_LHW7gr-lCV8ImPExEz26azA5xD1rDfOpABZWEKzgWQ