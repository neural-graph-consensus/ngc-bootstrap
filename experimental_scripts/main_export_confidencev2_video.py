import os
import cv2
import sys
import numpy as np
import torch as tr
import matplotlib.pyplot as plt
import torch.nn.functional as F
from tqdm import trange
from functools import partial
from neural_wrappers.callbacks import Callback, RandomPlotEachEpoch
from neural_wrappers.utilities import changeDirectory, minMax, toCategorical
from neural_wrappers.readers.datasets.carla.plot_utils import default, depthToImage, opticalFlowToImage
from neural_wrappers.graph import Node
from neural_wrappers.pytorch import trGetData
from media_processing_lib.image import toImage
from matplotlib.cm import viridis

sys.path.insert(0, "..")
from plot_utils import fToImage
from main import getArgs
from model import getNGC
from model.slanic.nodes import *

from readers import getReader
sys.path.append("%s/../../neural-graph-consensus" % (os.path.abspath(os.path.realpath(os.path.dirname(__file__)))))
from cycleconcepts.voting_algorithms import getVotingAlgorithm
from cycleconcepts.nodes import RGB

def redGreen(prediction1, prediction2, GT):
	prediction1 = np.argmax(prediction1, axis=-1)
	prediction2 = np.argmax(prediction2, axis=-1)
	GT = np.argmax(GT, axis=-1)

	green = (prediction1 == GT) & (prediction2 != GT)
	red = (prediction1 != GT) & (prediction2 == GT)
	blue = (prediction1 == GT) & (prediction2 == GT)
	black = (prediction1 != GT) & (prediction2 != GT)

	rgbImage = np.zeros((*prediction1.shape, 3), dtype=np.uint8)
	rgbImage[green] = [0, 255, 0]
	rgbImage[red] = [255, 0, 0]
	# rgbImage[blue] = [0, 0, 255]
	return rgbImage

def confidence(prediction, prior):
	maxPred = prediction.max(axis=-1)
	numBins = len(prior)
	Bin = np.int32(maxPred * numBins)
	conf = prior[Bin]
	confImage = np.stack([conf, conf, conf], axis=-1)
	confImage = np.uint8(confImage * 255)
	return confImage

def exportVideo(args, videoName):
	datasetType = args.dataCfg["type"]

	model = getNGC(args)
	model.eval()
	hParams = args.modelCfg["hyperParameters"]
	allNodes = {
		"rgb" : RGB(), "hsv" : HSV(), "depth1" : Depth1(**hParams["depth1"]), "edges1" : Edges1(), \
		"semanticgb1" : SemanticGB1(), "halftone1" : Halftone1(), "normals1" : Normals1(), "edges2" : Edges2(), \
		"opticalflow1" : OpticalFlow1(**hParams["opticalflow1"]), "semantic1" : Semantic1(**hParams["semantic1"]), \
		"semanticsegprop1" : SemanticSegProp1(**hParams["semanticsegprop1"]), "depth2" : Depth2(**hParams["depth2"])
	}
	reader = getReader(args, list(allNodes.values()), args.dataCfg["trainDir"], randomize=False)

	rgbNode = allNodes["rgb"]
	targetNode = model.edges[-1].outputNode
	strTargetNode = str(targetNode).split(" ")[0].lower()

	writer = None
	g = reader.iterate()
	Range = trange(0, len(g), desc="[VideoExportConfidenceV2:%s|Node:%s|Iter:%d]" % \
		(videoName, strTargetNode, args.iteration))
	prior = np.load("/scratch/nvme0n1/ngc/ngc-bootstrap/experimental_scripts/testHist/normed_th.npy")
	for i in Range:
		data = next(g)[0][0]
		# if i > 50:
		# 	break
		# data = g[100+i][0][0]
		if writer is None:
			MB, height, width = data["rgb"].shape[0 : 3]
			writer = cv2.VideoWriter(videoName, cv2.VideoWriter_fourcc(*'mp4v'), 29.97, (width * 3, height * 2))

		model.setNodesGroundTruth(trGetData(data))
		rgbImgs = data["rgb"]
		pseudoImgs = data[targetNode.groundTruthKey]
		results = model.npForward(data)
		voteResults = results["Vote"][0]
		redGreenResults = redGreen(prediction1=voteResults, prediction2=data["semantic1"], GT=data["semanticsegprop1"])
		confidenceResults = confidence(voteResults, prior)

		for j in range(MB):
			nodeImgs = {k : fToImage(data[k][j], node, datasetType) for k, node in allNodes.items()}
			voteImg = fToImage(voteResults[j], targetNode, datasetType)
			redGreenImg = redGreenResults[j]
			confImg = confidenceResults[j]

			firstRow = [nodeImgs["rgb"], nodeImgs["semantic1"], voteImg]
			secondRow = [redGreenImg, confImg, nodeImgs["semanticsegprop1"]]
			stack = np.concatenate([
				np.concatenate(firstRow, axis=1),
				np.concatenate(secondRow, axis=1)
			], axis=0)
			writer.write(stack[:, :, ::-1])
	writer.release()

def main():
	args = getArgs()
	exportVideo(args, "video_confidencev2.mp4")

if __name__ == "__main__":
	main()