import os
import cv2
import sys
import numpy as np
import torch as tr
import matplotlib.pyplot as plt
import torch.nn.functional as F
from tqdm import trange
from functools import partial
from neural_wrappers.callbacks import Callback, RandomPlotEachEpoch
from neural_wrappers.utilities import changeDirectory, minMax, toCategorical
from neural_wrappers.readers.datasets.carla.plot_utils import default, depthToImage, opticalFlowToImage
from neural_wrappers.graph import Node
from neural_wrappers.pytorch import trGetData
from media_processing_lib.image import toImage
from matplotlib.cm import viridis

sys.path.insert(0, "..")
from plot_utils import fToImage
from main import getArgs
from model import getNGC
from model.slanic.nodes import *

from readers import getReader
sys.path.append("%s/../../neural-graph-consensus" % (os.path.abspath(os.path.realpath(os.path.dirname(__file__)))))
from cycleconcepts.voting_algorithms import getVotingAlgorithm
from cycleconcepts.nodes import RGB

def exportVideo(args, videoName):
	datasetType = args.dataCfg["type"]

	model = getNGC(args)
	model.eval()
	hParams = args.modelCfg["hyperParameters"]
	allNodes = {
		"rgb" : RGB(), "hsv" : HSV(), "depth1" : Depth1(**hParams["depth1"]), "edges1" : Edges1(), \
		"semanticgb1" : SemanticGB1(), "halftone1" : Halftone1(), "normals1" : Normals1(), "edges2" : Edges2(), \
		"opticalflow1" : OpticalFlow1(**hParams["opticalflow1"]), "semantic1" : Semantic1(**hParams["semantic1"]), \
		"semanticsegprop1" : SemanticSegProp1(**hParams["semanticsegprop1"]), "depth2" : Depth2(**hParams["depth2"])
	}
	reader = getReader(args, list(allNodes.values()), args.dataCfg["trainDir"], randomize=False)

	rgbNode = allNodes["rgb"]
	targetNode = model.edges[-1].outputNode
	strTargetNode = str(targetNode).split(" ")[0].lower()

	writer = None
	g = reader.iterate()
	Range = trange(0, len(g), desc="[VideoExportCollage:%s|Node:%s|Iter:%d]" % \
		(videoName, strTargetNode, args.iteration))
	for i in Range:
		data = next(g)[0][0]
		# if i > 10:
		# 	break
		# data = g[100+i][0][0]
		if writer is None:
			MB, height, width = data["rgb"].shape[0 : 3]
			writer = cv2.VideoWriter(videoName, cv2.VideoWriter_fourcc(*'mp4v'), 29.97, (width * 4, height * 3))

		rgbImgs = data["rgb"]
		pseudoImgs = data[targetNode.groundTruthKey]
		for j in range(MB):
			nodeImgs = {k : fToImage(data[k][j], node, datasetType) for k, node in allNodes.items()}
			firstRow = [nodeImgs["rgb"], nodeImgs["hsv"], nodeImgs["semanticgb1"], nodeImgs["halftone1"]]
			secondRow = [nodeImgs["depth1"], nodeImgs["opticalflow1"], nodeImgs["edges1"], nodeImgs["edges2"]]
			thirdRow = [nodeImgs["depth2"], nodeImgs["normals1"], nodeImgs["semantic1"], nodeImgs["semanticsegprop1"]]

			stack = np.concatenate([
				np.concatenate(firstRow, axis=1),
				np.concatenate(secondRow, axis=1),
				np.concatenate(thirdRow, axis=1)
			], axis=0)
			writer.write(stack[:, :, ::-1])
	writer.release()

def main():
	args = getArgs()
	exportVideo(args, "video_collage.mp4")

if __name__ == "__main__":
	main()