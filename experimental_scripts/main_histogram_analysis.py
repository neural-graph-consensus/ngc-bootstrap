import os
import sys
import numpy as np
import torch as tr
import matplotlib.pyplot as plt
import torch.nn.functional as F
from functools import partial
from neural_wrappers.callbacks import Callback, RandomPlotEachEpoch
from neural_wrappers.utilities import changeDirectory
from neural_wrappers.readers.datasets.carla.plot_utils import default, depthToImage, opticalFlowToImage
from neural_wrappers.graph import Node

sys.path.insert(0, "..")
sys.path.append("%s/../../neural-graph-consensus" % (os.path.abspath(os.path.realpath(os.path.dirname(__file__)))))

from plot_utils import fToImage, slanicSemantic1
from main import getArgs, getParser
from model import getNGC, getSingleLinks
from readers import getReader
from cycleconcepts.voting_algorithms import getVotingAlgorithm
from cycleconcepts.nodes import RGB

# Takes the _correct_ class and computes the confidence for when the network predicts correctly and wrongly
class Histogram1(Callback):
	def __init__(self, nBins:int, Key:str):
		super().__init__()
		self.nBins = nBins
		self.Key = Key
		self.trueHistogram = np.zeros((nBins, ), dtype=np.int32)
		self.falseHistogram = np.zeros((nBins, ), dtype=np.int32)

	def updateHistogramsOfConfidence(self, y, t):
		# Get the highest confidence
		yIsMax = y >= y.max(axis=-1, keepdims=True)
		Where = t == 1
		y = y[Where]
		yIsMax = yIsMax[Where]

		HPos = (y[yIsMax] * self.nBins).astype(int)
		HNeg = (y[~yIsMax] * self.nBins).astype(int)
		HPos = np.bincount(HPos)
		HNeg = np.bincount(HNeg)

		self.trueHistogram[0 : len(HPos)] += HPos
		self.falseHistogram[0 : len(HNeg)] += HNeg

	def plotHistogramsOfConfidence(self):
		ax = plt.subplots(2, 2, figsize=(10, 10))[1]
		Range = np.arange(self.nBins)
		ax[0,0].bar(Range, self.trueHistogram, width=0.95)
		ax[0,0].set_xlabel("Confidence")
		ax[0,0].set_ylabel("Count")
		ax[0,0].set_title("TP")

		ax[0, 1].bar(Range, 100 * self.trueHistogram / self.trueHistogram.sum(), width=0.95)
		ax[0, 1].set_xlabel("Confidence")
		ax[0, 1].set_ylabel("Count (%)")
		ax[0, 1].set_title("TP (%)")

		ax[1, 0].bar(Range, self.falseHistogram, width=0.95)
		ax[1, 0].set_xlabel("Confidence")
		ax[1, 0].set_ylabel("Count")
		ax[1, 0].set_title("FP")

		ax[1, 1].bar(Range, 100 * self.falseHistogram / self.falseHistogram.sum(), width=0.95)
		ax[1, 1].set_xlabel("Confidence")
		ax[1, 1].set_ylabel("Count (%)")
		ax[1, 1].set_title("FP (%)")

		# plt.show()
		plt.savefig("hist1_tp_fp.png", dpi=300)

	def onIterationEnd(self, results, labels, **kwargs):
		t = labels["semantic1"]
		y = results[self.Key][0]
		self.updateHistogramsOfConfidence(y, t)
	
	def onEpochEnd(self, **kwargs):
		self.plotHistogramsOfConfidence()

# Takes the _correct_ class and computes the confidence for when the network predicts correctly and wrongly
class Histogram2(Callback):
	def __init__(self, nBins:int, Key:str):
		super().__init__()
		self.nBins = nBins
		self.Key = Key
		self.trueHistogram = np.zeros((nBins, ), dtype=np.int32)
		self.falseHistogram = np.zeros((nBins, ), dtype=np.int32)

	def updateHistogramsOfConfidence(self, y, t):
		# Get the highest confidence
		assert len(np.unique(t)) == 2
		maxPred = y.max(axis=-1, keepdims=True)
		yIsMax = y >= maxPred
		# Pixels with values of 1 represent where we predicted the correct class (as max confidence) and 0 where the max
		#  confidence predicted the wrong class
		correctMask = (yIsMax * t).sum(axis=-1)
		assert len(np.unique(correctMask)) == 2

		whereCorrect = np.where(correctMask == 1)
		whereWrong = np.where(correctMask == 0)

		maxPredCorrect = maxPred[whereCorrect][:, 0]
		maxPredWrong = maxPred[whereWrong][:, 0]

		HPos = (maxPredCorrect * self.nBins).astype(int)
		HPos = np.bincount(HPos)
		self.trueHistogram[0 : len(HPos)] += HPos

		HNeg = (maxPredWrong * self.nBins).astype(int)
		HNeg = np.bincount(HNeg)
		self.falseHistogram[0 : len(HNeg)] += HNeg

	def plotAsHistograms(self):
		Range = np.arange(self.nBins)
		ax = plt.subplots(2, 2, figsize=(10, 10))[1]

		ax[0,0].bar(Range, self.trueHistogram, width=0.95)
		ax[0,0].set_xlabel("Confidence")
		ax[0,0].set_ylabel("Count")
		ax[0,0].set_title("Max @ Correct predictions")

		ax[0, 1].bar(Range, 100 * self.trueHistogram / self.trueHistogram.sum(), width=0.95)
		ax[0, 1].set_xlabel("Confidence")
		ax[0, 1].set_ylabel("Count (%)")
		ax[0, 1].set_title("Max @ Correct predictions (%)")

		ax[1, 0].bar(Range, self.falseHistogram, width=0.95)
		ax[1, 0].set_xlabel("Confidence")
		ax[1, 0].set_ylabel("Count")
		ax[1, 0].set_title("Max @ Wrong predictions")

		ax[1, 1].bar(Range, 100 * self.falseHistogram / self.falseHistogram.sum(), width=0.95)
		ax[1, 1].set_xlabel("Confidence")
		ax[1, 1].set_ylabel("Count (%)")
		ax[1, 1].set_title("Max @ Worrect predictions (%)")

		# plt.show()
		plt.savefig("hist2_tp_fp.png", dpi=300)
		np.save("trueHistogram.npy", self.trueHistogram)
		np.save("falseHistogram.npy", self.falseHistogram)

	def plotOverlapped(self):
		X = np.arange(self.nBins)
		A, B = self.trueHistogram.sum(), self.falseHistogram.sum()
		Sum = A + B
		trueHistogram = self.trueHistogram
		falseHistogram = self.falseHistogram

		DenominatorEqualPrior = (trueHistogram / A + falseHistogram / B) / 2
		TH = trueHistogram / (DenominatorEqualPrior + np.spacing(1))
		normedTH = TH / TH.max()

		plt.figure()
		plt.plot(X, trueHistogram / A * 100, label="Correct")
		plt.plot(X, falseHistogram / B * 100, label="Wrong")
		plt.xlabel("Confidence")
		plt.ylabel("Count (%)")
		plt.legend()
		plt.savefig("hist2_plot.png")

		plt.figure()
		plt.plot(X, trueHistogram / Sum * 100, label="Correct")
		plt.plot(X, falseHistogram / Sum * 100, label="Wrong")
		plt.xlabel("Confidence")
		plt.ylabel("Count (%)")
		plt.legend()
		plt.savefig("hist2_plot_normed.png")

		plt.figure()
		plt.plot(normedTH, label="Combined (equal prior)")
		plt.xlabel("Confidence")
		plt.ylabel("Count (%)")
		plt.legend()
		plt.savefig("hist2_plot_combined_equal_prior.png")
		np.save("normed_th.npy", normedTH)

	def onIterationEnd(self, results, labels, **kwargs):
		t = labels["semantic1"]
		y = results[self.Key][0]
		self.updateHistogramsOfConfidence(y, t)

	def onEpochEnd(self, **kwargs):
		self.plotAsHistograms()
		self.plotOverlapped()

def _getArgs():
	parser = getParser()
	parser.add_argument("--nBins", type=int, required=True)
	parser.add_argument("--ngcOrSingleLink", required=True)
	args = getArgs(parser)
	assert args.ngcOrSingleLink in ("ngc", "single_link")
	return args

def main():
	args = _getArgs()
	if args.ngcOrSingleLink == "ngc":
		model = getNGC(args)
		Key = "Vote"
	else:
		model = next(getSingleLinks(args))
		model.loadWeights("%s/%s/model_best.pkl" % (args.modelCfg["modelWorkingDir"], args.whichSingleLink))
		Key = "RGB (ID: 0) -> Semantic1 (ID: 1)"
	model.eval()
	reader = getReader(args, model.getNodes(), args.dataCfg["testDir"], randomize=False)

	changeDirectory(os.path.abspath(os.path.dirname(__file__)) + "/testHist")
	model.addCallback(Histogram2(nBins=args.nBins, Key=Key))
	model.testGenerator(reader.iterate())

if __name__ == "__main__":
	main()