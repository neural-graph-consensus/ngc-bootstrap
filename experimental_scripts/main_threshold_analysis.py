import os
import sys
import numpy as np
import torch as tr
import matplotlib.pyplot as plt
import torch.nn.functional as F
from functools import partial
from neural_wrappers.callbacks import Callback, RandomPlotEachEpoch
from neural_wrappers.utilities import changeDirectory
from neural_wrappers.readers.datasets.carla.plot_utils import default, depthToImage, opticalFlowToImage
from neural_wrappers.graph import Node

sys.path.insert(0, "..")
sys.path.append("%s/../../neural-graph-consensus" % (os.path.abspath(os.path.realpath(os.path.dirname(__file__)))))

from main import getArgs, getParser
from model import getNGC
from readers import getReader
from cycleconcepts.voting_algorithms import getVotingAlgorithm
from cycleconcepts.nodes import RGB

def f(obj, x, singleLinkKey, threshold, voteFunction):
	SL = x[singleLinkKey]
	vote = voteFunction(obj, x)

	Max = vote.max(dim=-1)
	OneHotVote = F.one_hot(Max[1], 11).type(tr.bool)
	Mask = vote[OneHotVote] >= threshold
	Mask = Mask.reshape(*vote.shape[0 : -1], 1)

	res = vote * Mask + SL * ~Mask
	return res

def _getArgs():
	parser = getParser()
	parser.add_argument("--N", type=int, required=True)
	return getArgs(parser)

def main():
	args = _getArgs()
	model = getNGC(args)
	model.eval()
	origVoteFunction = getVotingAlgorithm(args.modelCfg["graph"]["votingAlgorithm"])
	singleLinkKey = "RGB (ID: 0) -> Semantic1 (ID: 1)"
	# singleLinkKey = "HSV (ID: 0) -> Semantic1 (ID: 1)"

	reader = getReader(args, model.getNodes(), args.dataCfg["testDir"], randomize=False)
	changeDirectory(os.path.abspath(os.path.dirname(__file__)) + "/testThreshold")

	LS = np.linspace(0, 1, args.N)
	accs = np.zeros((len(LS), ))
	for i in range(args.N):
		voteFn = partial(f, singleLinkKey=singleLinkKey, threshold=LS[i], voteFunction=origVoteFunction)
		model.setVoteFunction(voteFn)

		res = model.testGenerator(reader.iterate())
		res = res["Test"]["Vote"]
		res = {str(k) : res[k] for k in res}

		accs[i] = res["Accuracy (global)"]

	print("Acc:", list(accs))
	print("Best: %s" % LS[np.argmax(accs)])
	np.save("accs.npy", accs)
	plt.figure()
	plt.xlabel("Threshold")
	plt.ylabel("Accuracy")
	plt.title("Single link: %2.4f. NGC: %2.4f" % (accs[-1], accs[0]))
	plt.plot(LS, accs)
	plt.show()
	# plt.savefig("accs.png")

if __name__ == "__main__":
	main()