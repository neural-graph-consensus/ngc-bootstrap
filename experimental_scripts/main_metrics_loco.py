import sys
import numpy as np
from argparse import ArgumentParser
from pathlib import Path
from natsort import natsorted
from neural_wrappers.utilities import fullPath
from neural_wrappers.metrics import accuracy
from neural_wrappers.metrics.f1score import GlobalF1Score
from neural_wrappers.metrics.mean_iou import GlobalMeanIoU

sys.path.append("..")

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("gtDir")
    parser.add_argument("predictionsDir")
    args = parser.parse_args()
    return args

def getPseudoLabels(args):
    y = natsorted([fullPath(x) for x in Path(args.predictionsDir).glob("*.npz")])
    t = natsorted([fullPath(x) for x in Path(args.gtDir).glob("*.npz")])
    assert len(y) > 0 and len(y) == len(t)
    y = np.array([np.load(x)["arr_0"] for x in y]).astype(np.float32)
    t = np.array([np.load(x)["arr_0"] for x in t]).astype(np.float32)
    assert len(np.unique(y)) == 2
    assert len(np.unique(t)) == 2
    return y, t

def main():
    args = getArgs()
    y, t = getPseudoLabels(args)
    print(y.shape, t.shape)
    MB = 33
    numSteps = len(y) // MB + (len(y) % MB != 0)

    f1 = GlobalF1Score()
    for i in range(numSteps):
        startIx, endIx = i * MB, min((i + 1) * MB, len(y))
        f1(y[startIx : endIx], t[startIx : endIx])

    print("TP: %s" % f1.globalScores.TP)
    print("FP: %s" % f1.globalScores.FP)
    print("FN: %s" % f1.globalScores.FN)
    print("TN: %s" % f1.globalScores.TN)

    TP, FP, FN, TN = f1.globalScores.fReturn()

    precision = TP / (TP + FP + np.spacing(1))
    recall = TP / (TP + FN + np.spacing(1))
    resF1 = 2 * precision * recall / (precision + recall + np.spacing(1))
    print("F1: %s. Mean: %2.5f" % (resF1, resF1.mean()))

    # resAcc = (TP + TN) / (TP + FP + FN + TN)
    # print("Acc: %s. Mean: %2.5f" % (resAcc, resAcc.mean()))

    resIoU = TP / (TP + FP + FN)
    print("IoU: %s. Mean: %2.5f" % (resIoU, resIoU.mean()))

    # # mIoU = GlobalMeanIoU()
    # # mIoU(y, t)
    # # resMIoU = mIoU.epochReduceFunction(None)
    # # print("mIoU: %s. Mean: %2.5f" % (resMIoU, resMIoU.mean()))

    resAcc = accuracy(y, t)
    print("Accuracy: %2.5f%%" % (resAcc * 100))

if __name__ == "__main__":
    main()
