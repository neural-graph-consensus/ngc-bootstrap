import os
import cv2
import sys
import numpy as np
import torch as tr
import matplotlib.pyplot as plt
import torch.nn.functional as F
from tqdm import trange
from functools import partial
from neural_wrappers.callbacks import Callback, RandomPlotEachEpoch
from neural_wrappers.utilities import changeDirectory, minMax
from neural_wrappers.readers.datasets.carla.plot_utils import default, depthToImage, opticalFlowToImage
from neural_wrappers.graph import Node
from neural_wrappers.pytorch import trGetData
from media_processing_lib.image import toImage
from matplotlib.cm import viridis

sys.path.insert(0, "..")
from plot_utils import fToImage
from main import getArgs
from model import getNGC
from readers import getReader
sys.path.append("%s/../../neural-graph-consensus" % (os.path.abspath(os.path.realpath(os.path.dirname(__file__)))))
from cycleconcepts.voting_algorithms import getVotingAlgorithm
from cycleconcepts.nodes import RGB

def toImageVar(x):
	x = minMax(x)
	x = viridis(x)
	x = toImage(x)
	return x

def getVariance(results):
	results = np.concatenate([*results.values()][0:-1], axis=0)
	Shape = results.shape

	# Variances of the _max_ class
	# results = results.max(axis=-1).var(axis=0)

	# Variance of all classes, meaned at end
	results = results.reshape(results.shape[0], -1, results.shape[-1])
	results = results.var(0).mean(-1).reshape(Shape[1:-1])
	return results

def exportVideo(args, videoName):
	datasetType = args.dataCfg["type"]

	model = getNGC(args)
	model.eval()
	reader = getReader(args, model.getNodes(), args.dataCfg["trainDir"], randomize=False)

	rgbNode = RGB()
	targetNode = model.edges[-1].outputNode
	strTargetNode = str(targetNode).split(" ")[0].lower()

	writer = None
	g = reader.iterate()
	Range = trange(0, len(g), desc="[VideoExportConfidence:%s|Node:%s|Iter:%d]" % \
		(videoName, strTargetNode, args.iteration))
	for i in Range:
		data = next(g)[0][0]
		# if i > 10:
		# 	break
		# data = g[100+i][0][0]
		if writer is None:
			height, width = data["rgb"].shape[1 : 3]
			writer = cv2.VideoWriter(videoName, cv2.VideoWriter_fourcc(*'mp4v'), 29.97, (width * 2, height * 2))

		model.setNodesGroundTruth(trGetData(data))

		rgbImgs = data["rgb"]
		pseudoImgs = data[targetNode.groundTruthKey]
		results = model.npForward(data)
		voteResults = results["Vote"][0]
		variances = getVariance(results)
		# Mean variance for _all_ classes independently.
		MB = len(voteResults)

		for j in range(MB):
			rgbImg =  fToImage(rgbImgs[j], rgbNode, datasetType)
			pseudoImg = fToImage(pseudoImgs[j], targetNode, datasetType)
			firstRow = np.concatenate([rgbImg, pseudoImg], axis=1)

			voteImg = fToImage(voteResults[j], targetNode, datasetType)
			voteVariance = toImageVar(variances[j])
			secondRow = np.concatenate([voteVariance, voteImg], axis=1)

			stack = np.concatenate([firstRow, secondRow], axis=0)
			writer.write(stack[:, :, ::-1])
	writer.release()

def main():
	args = getArgs()
	exportVideo(args, "video_variance.mp4")

if __name__ == "__main__":
	main()