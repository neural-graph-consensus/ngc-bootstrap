import os
import cv2
import sys
import numpy as np
import torch as tr
import matplotlib.pyplot as plt
import torch.nn.functional as F
from tqdm import trange
from functools import partial
from neural_wrappers.callbacks import Callback, RandomPlotEachEpoch
from neural_wrappers.utilities import changeDirectory
from neural_wrappers.readers.datasets.carla.plot_utils import default, depthToImage, opticalFlowToImage
from neural_wrappers.graph import Node
from neural_wrappers.pytorch import trGetData
from media_processing_lib.image import toImage

sys.path.insert(0, "..")
sys.path.append("%s/../../neural-graph-consensus" % (os.path.abspath(os.path.realpath(os.path.dirname(__file__)))))

from plot_utils import fToImage
from main import getArgs
from model import getNGC
from readers import getReader
from cycleconcepts.voting_algorithms import getVotingAlgorithm
from cycleconcepts.nodes import RGB

def toImageConf(x):
	x = x.max(axis=-1, keepdims=1)
	x = (x * 255).astype(np.uint8)
	x = np.repeat(x, 3, axis=-1)
	return x

def exportVideo(args, videoName):
	datasetType = args.dataCfg["type"]

	model = getNGC(args)
	model.eval()
	reader = getReader(args, model.getNodes(), args.dataCfg["trainDir"], randomize=False)

	rgbNode = RGB()
	targetNode = model.edges[-1].outputNode
	strTargetNode = str(targetNode).split(" ")[0].lower()

	writer = None
	g = reader.iterate()
	Range = trange(0, len(g), desc="[VideoExportConfidence:%s|Node:%s|Iter:%d]" % \
		(videoName, strTargetNode, args.iteration))
	for i in Range:
		data = next(g)[0][0]
		if writer is None:
			height, width = data["rgb"].shape[1 : 3]
			writer = cv2.VideoWriter(videoName, cv2.VideoWriter_fourcc(*'mp4v'), 29.97, (width * 4, height * 2))

		model.setNodesGroundTruth(trGetData(data))
		rgbImgs = data["rgb"]
		pseudoImgs = data[targetNode.groundTruthKey]
		results = model.npForward(data)
		voteResults = results["Vote"][0]
		slResults = results["RGB (ID: 0) -> Semantic1 (ID: 1)"][0]
		MB = len(voteResults)

		for j in range(MB):
			rgbImg =  fToImage(rgbImgs[j], rgbNode, datasetType)
			pseudoImg = fToImage(pseudoImgs[j], targetNode, datasetType)
			slImg = fToImage(slResults[j], targetNode, datasetType)
			voteImg = fToImage(voteResults[j], targetNode, datasetType)
			firstRow = np.concatenate([rgbImg, pseudoImg, slImg, voteImg], axis=1)

			pseudoConfidence = toImageConf(pseudoImgs[j])
			voteConfidence = toImageConf(voteResults[j])
			slConfidence = toImageConf(slResults[j])
			secondRow = np.concatenate([rgbImg * 0, pseudoConfidence, slConfidence, voteConfidence], axis=1)

			stack = np.concatenate([firstRow, secondRow], axis=0)
			writer.write(stack[:, :, ::-1])
	writer.release()

def main():
	args = getArgs()
	exportVideo(args, "video_confidence.mp4")

if __name__ == "__main__":
	main()