import cv2
import numpy as np
from tqdm import trange
from neural_wrappers.pytorch import trGetData
from cycleconcepts.nodes import RGB

from model import getNGC
from readers import getReader
from plot_utils import fToImage

def exportVideo(args, videoName):
	datasetType = args.dataCfg["type"]
	model = getNGC(args)
	model.eval()
	reader = getReader(args, model.getNodes(), args.dataCfg["trainDir"], randomize=False)

	rgbNode = RGB()
	targetNode = model.edges[-1].outputNode
	strTargetNode = str(targetNode).split(" ")[0].lower()

	writer = None
	g = reader.iterate()
	Range = trange(0, len(g), desc="[VideoExportConfidence:%s|Node:%s|Iter:%d]" % \
		(videoName, strTargetNode, args.iteration))
	for i in Range:
		data = next(g)[0][0]
		if writer is None:
			height, width = data["rgb"].shape[1 : 3]
			writer = cv2.VideoWriter(videoName, cv2.VideoWriter_fourcc(*'mp4v'), 29.97, (width * 3, height))
		model.setNodesGroundTruth(trGetData(data))

		rgbImgs = data["rgb"]
		gtImgs = data[targetNode.groundTruthKey]
		voteResults = model.npForward(data)["Vote"][0]
		MB = len(voteResults)

		for j in range(MB):
			inImg =  fToImage(rgbImgs[j], rgbNode, datasetType)
			GTImg = fToImage(gtImgs[j], targetNode, datasetType)
			outImg = fToImage(voteResults[j], targetNode, datasetType)
			stack = np.concatenate([inImg, GTImg, outImg], axis=1)
			writer.write(stack[:, :, ::-1])
	writer.release()

def main():
	from main import getArgs
	args = getArgs()
	exportVideo(args)

if __name__ == "__main__":
	main()