import numpy as np
from pathlib import Path
from tqdm import trange
from typing import Optional
from neural_wrappers.pytorch import trGetData

from model import getNGC
from readers import getReader

def exportPseudolabels(args, threshold:Optional[float]=None):
	model = getNGC(args, threshold)
	model.eval()
	reader = getReader(args, model.getNodes(), args.dataCfg["trainDir"], randomize=False)

	pseudoLabelsBaseDir = args.dataCfg["pseudoLabelsDir"]
	assert pseudoLabelsBaseDir.exists()
	outNode = model.edges[0].getNodes()[1]
	outNodeStr = args.modelCfg["mainNode"]
	pseudoLabelsDir = Path("%s/%s" % (pseudoLabelsBaseDir, outNodeStr))
	pseudoLabelsDir.mkdir(exist_ok=True)

	if args.dataCfg["type"] == "Dummy":
		makeNpy = {outNode.groundTruthKey : (lambda x : x)}
	elif args.dataCfg["type"] == "Slanic":
		makeNpy = {
			"semantic1" : lambda x : x
		}
	else:
		assert False
	print("[exportPseudolabels] Pseudo labels dir: %s" % pseudoLabelsDir)

	g = reader.iterate()
	N = len(g)
	N = 5
	StrThr = "None" if threshold is None else "%2.2f" % threshold
	cnt = 0
	for i in trange(N, desc="Pseudolabels-%s-%d-thr%s" % (outNodeStr, args.iteration, StrThr)):
		data = next(g)[0][0]
		model.setNodesGroundTruth(trGetData(data))
		voteResults = model.npForward(data)["Vote"][0]
		MB = len(voteResults)
	
		pseudoLabels = makeNpy[outNode.groundTruthKey](voteResults)
		for j in range(MB):
			outFile = "%s/%d.npz" % (pseudoLabelsDir, cnt)
			np.savez_compressed(outFile, pseudoLabels[j])
			cnt += 1