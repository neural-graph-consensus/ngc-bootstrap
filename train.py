from neural_wrappers.utilities import changeDirectory
from neural_wrappers.graph import Graph

from model import getSingleLinks
from readers import getReader

def trainSingleLinks(args):
	for model in getSingleLinks(args):
		trainModel(args, model)

def trainModel(args, model:Graph):
	print("Training %s iteration %d" % (model.name, args.iteration))
	if args.debug:
		print("[trainModel] Debug mode. Setting num epochs to 5.")
		args.trainCfg["numEpochs"] = 5

	trainGenerator = getReader(args, model.getNodes(), args.dataCfg["trainDir"], randomize=True).iterate()
	valGenerator = None if not args.dataCfg["validationDir"] else \
		getReader(args, model.getNodes(), args.dataCfg["validationDir"], randomize=False).iterate()

	modelDir = "%s/%s" % (args.modelCfg["modelWorkingDir"], model.name)
	changeDirectory(modelDir)
	model.trainGenerator(trainGenerator, args.trainCfg["numEpochs"], valGenerator)
	changeDirectory(args.workingDir)