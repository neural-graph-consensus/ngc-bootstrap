import numpy as np
from tqdm import trange
from functools import partial
from typing import Optional
from neural_wrappers.graph import Graph
from neural_wrappers.utilities import changeDirectory
from neural_wrappers.callbacks import Callback, CallbackName
from cycleconcepts.models import SingleLinkGraph

from readers import getReader
from model import getSingleLinks, getNGC, getRGBSingleLink

def testModel(args, model:Graph):
	reader = getReader(args, model.getNodes(), args.dataCfg["testDir"], randomize=False)

	if not isinstance(model, SingleLinkGraph):
		modelName = "NGC-%s" % (model.edges[-1].getNodes()[1].groundTruthKey)
	else:
		modelName = model.name

	print("Testing %s iteration %d" % (modelName, args.iteration))

	modelTestDir = "%s/%s" % (args.dataCfg["testResultsDir"], modelName)
	changeDirectory(modelTestDir)
	model.eval()
	res = model.testGenerator(reader.iterate())
	changeDirectory(args.workingDir)
	return res["Test"]

def testRGBLink(args):
	modelName = "rgb_%s" % args.modelCfg["mainNode"]
	args.whichSingleLink = modelName
	model = getRGBSingleLink(args)
	model.loadWeights("%s/%s/model_best.pkl" % (args.modelCfg["modelWorkingDir"], modelName))
	return testModel(args, model)

def testNGC(args):
	model = getNGC(args)
	return testModel(args, model)

def fPrint(result):
	_Str = ""
	if isinstance(result, np.number):
		return "\n  - %s" % result

	for k in result:
		if k == "duration":
			continue
		res = result[k]
		if isinstance(res, (np.ndarray, np.number)):
			_Str += "\n  - %s: %s" % (k, str(res))
			if not isinstance(res, np.number):
				_Str += "\n  - %s (mean): %s" % (k, str(res.mean()))
		elif isinstance(res, dict):
			_Str += fPrint(res)
		else:
			breakpoint()
	return _Str

def testAndExportResults(args, fileName:str):
	if not "testDir" in args.dataCfg or args.dataCfg["testDir"] is None:
		print("[testAndExportResult] No test set found. Skipping.")
		return

	print("[testAndExportResult] Using test set from: %s." % args.dataCfg["testDir"])
	resultSL = testRGBLink(args)
	mainLink = "rgb2%s" % args.modelCfg["mainNode"]
	resultGraph = testNGC(args)

	np.set_printoptions(precision=4, suppress=True)
	Str = "_____________"
	Str += "\n----Results (model: %s. iteration: %d)-----" % (mainLink, args.iteration)

	Str += "\nSingle link:"
	Str += fPrint(resultSL)

	Str += "\nGraph:" % resultGraph
	for edge in resultGraph:
		if edge == "duration":
			continue
		Str += "\nEdge: %s" % edge
		Str += fPrint(resultGraph[edge])

	Str += "\n_____________\n"
	print(Str)

	fileName = "%s/%s" % (args.dataCfg["testResultsDir"], fileName.split("/")[-1])
	print("[testAndExportResult] Exporting to: %s" % fileName)
	f = open(fileName, "a")
	f.write(Str)
	f.flush()
	f.close()

def testSingleLink(args):
	# assert args.whichSingleLink in args.modelCfg["graph"]["edges"]
	hyperParameters = args.modelCfg["hyperParameters"]
	modelName = args.whichSingleLink
	model = next(getSingleLinks(args))
	model.loadWeights("%s/%s/model_best.pkl" % (args.modelCfg["modelWorkingDir"], modelName))
	return testModel(args, model)

# @brief Test all single links and return a numpy array of the results.
def testSingleLinks(args):
	graphEdges = args.modelCfg["graph"]["edges"]
	assert len(graphEdges) >= 2, "Expected at least 2 edges for this feature. Found: %s" % graphEdges
	assert "relevantMetric" in args.modelCfg

	# Get the individual single link results
	slResults = []
	metricNames = None
	for i in trange(len(graphEdges), desc="Single links"):
		modelName = graphEdges[i]
		args.whichSingleLink = modelName
		slResult = testSingleLink(args)
		slResult.pop(CallbackName("Loss"))
		slResult.pop("duration")
		slResult = tuple(slResult.values())
		assert len(slResult) == 1
		slResult = slResult[0]
		if i == 0:
			metricNames = list(slResult.keys())
		else:
			for a, b in zip(metricNames, slResult.keys()):
				assert a == b
		slResult = list(slResult.values())
		slResult = np.array([x.mean() if isinstance(x, np.ndarray) else x for x in slResult])
		slResults.append(slResult)
	return np.array(slResults)