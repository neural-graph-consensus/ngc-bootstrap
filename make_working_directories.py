import os
from pathlib import Path

def fullPath(x):
	return Path(os.path.realpath(os.path.abspath(x))) if not x is None else None

def getUniqueRepresentations(path):
	labelDirs = filter(lambda y : y.is_dir(), path.iterdir())
	uniqueRepresentations = [str(x).split("/")[-1] for x in labelDirs]
	return uniqueRepresentations

def makeDataDirectories(dataCfg):
	Path(dataCfg["dataWorkingDir"]).mkdir(parents=True, exist_ok=True)
	dataCfg["originalTrainDir"] = fullPath(dataCfg["trainDir"])
	dataCfg["originalValidationDir"] = fullPath(dataCfg["validationDir"])
	dataCfg["originalTestDir"] = fullPath(dataCfg["testDir"])

	def fMake(dirType:str):
		# Train => trainDir
		dataCfgKey = "%sDir" % dirType.lower()
		# Train => originalTrainDir
		dataOriginalKey = "original%sDir" % dirType
		# Train => dataDir/train
		dataLinkedDir = "%s/%s" % (dataCfg["dataWorkingDir"], dirType.lower())
		uniqueRepresentations = getUniqueRepresentations(dataCfg[dataOriginalKey])

		print("[makeDataDirectories] %s dir: %s." % (dirType, dataCfg[dataOriginalKey]))
		print("[makeDataDirectories]   - Unique representations: %s." % uniqueRepresentations)
		print("[makeDataDirectories]   - Making %s dir symlink in data working directory." % dirType)
		# dataCfg["validationDir"] = "%s/validation" % dataCfg["dataWorkingDir"]
		dataCfg[dataCfgKey] = dataLinkedDir
		if not Path(dataCfg[dataCfgKey]).exists():
			os.symlink(dataCfg[dataOriginalKey], dataCfg[dataCfgKey])
		else:
			print("[makeDataDirectories]   - Symlink %s already exists." % dataCfg[dataCfgKey])

	fMake("Train")

	if dataCfg["originalValidationDir"] is None:
		print("[makeDataDirectories] Not using validation dir")
	else:
		fMake("Validation")

	if dataCfg["originalTestDir"] is None:
		print("[makeDataDirectories] Not using test dir")
	else:
		fMake("Test")

	dataCfg["pseudoLabelsDir"] = fullPath("%s/exported_pseudolabels" % dataCfg["dataWorkingDir"])
	dataCfg["pseudoLabelsDir"].mkdir(exist_ok=True)
	print("[makeDataDirectories] To be exported pseudolabels dir: %s" % dataCfg["pseudoLabelsDir"])

	print("[makeDataDirectories] Done.")
	return dataCfg

def getNextIterationBaseDirs(dataCfg, baseWorkingDir, iteration):
	prevWorkingDir = fullPath("%s/iter%d" % (baseWorkingDir, iteration - 1))
	prevPseudolabelsDir = fullPath("%s/data/exported_pseudolabels" % prevWorkingDir)
	assert prevPseudolabelsDir.exists(), "Pseudolabels dir %s does not exist." % prevPseudolabelsDir
	dataCfg["originalTrainDir"] = prevPseudolabelsDir
	return dataCfg

def checkIterationPseudolabels(args):
	pass
	# # Check that all pseudolabels are available
	# thisModelStr = args.modelCfg["mainLink"]
	# thisNodes = thisModelStr.split("2")
	# expectedCount = len(list(Path("%s/data/trainDir/stationary" % args.dir).glob("*.npy")))
	# if args.iteration > 0:
	# 	pseudoLabelsDir = "%s/data/trainDir/iter%d" % (args.dir, args.iteration)
	# 	for graphModelStr in args.modelCfg["graph"][1:]:
	# 		otherNodes = graphModelStr.split("2")
	# 		for node in otherNodes:
	# 			if node == args.modelCfg["stationary"]:
	# 				continue
	# 			count = len(list(Path(pseudoLabelsDir).glob("*%s*" % node)))
	# 			assert count == expectedCount, ("Node '%s' has not produced pseudolabels at iteration %d. " + \
	# 				"Expected: %d. Got: %d") % (node, args.iteration-1, expectedCount, count)

	# return args


def makeWorkingDirectories(args):
	# First and foremost, make sure we work with absolute paths, so any changeDirectory call
	#  does not mess up everything.
	assert not args.dir is None
	args.dir = fullPath(args.dir)
	args.workingDir = fullPath("%s/iter%d" % (args.dir, args.iteration))
	print("[makeWorkingDirectories] Base directory: %s. Iteration: %d." % (args.dir, args.iteration))
	print("[makeWorkingDirectories] Working directory: %s." % args.workingDir)

	# This is where we control if for iteration 0 the directory must not exist.
	if args.iteration == 0:
		if args.exist_ok == False:
			assert not Path(args.dir).exists()
			assert not Path(args.workingDir).exists()
		Path(args.workingDir).mkdir(exist_ok=True, parents=True)
	else:
		args.dataCfg = getNextIterationBaseDirs(args.dataCfg, args.dir, args.iteration)
	checkIterationPseudolabels(args)

	args.dataCfg["dataWorkingDir"] = fullPath("%s/data" % args.workingDir)
	print("[makeWorkingDirectories] Data Working directory: %s" % args.dataCfg["dataWorkingDir"])
	args.dataCfg = makeDataDirectories(args.dataCfg)

	# For exporting results on test set.
	args.dataCfg["testResultsDir"] = fullPath("%s/test_results" % args.workingDir)

	args.modelCfg["modelWorkingDir"] = fullPath("%s/models/%s" % (args.workingDir, args.modelCfg["mainNode"]))
	args.modelCfg["modelWorkingDir"].mkdir(parents=True, exist_ok=True)
	print("[makeWorkingDirectories] Model Working directory: %s" % args.modelCfg["modelWorkingDir"])
	return args