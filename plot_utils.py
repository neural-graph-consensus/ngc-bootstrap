import os
import sys
import numpy as np
from pathlib import Path
from media_processing_lib.image import tryWriteImage
from neural_wrappers.graph import Node
from collections import OrderedDict

sys.path.append("%s/../neural-graph-consensus" % (os.path.abspath(os.path.realpath(os.path.dirname(__file__)))))
from cycleconcepts.models import SingleLinkGraph, GraphEnsemble

def slanicSemantic1(x):
	x = np.argmax(x, axis=-1)
	labels = ["land", "forest", "residential", "road", "church", "cars", "water", "sky", "hill", "person", "fence"]
	colors = [(0, 255, 0), (0, 127, 0), (255, 255, 0), (255, 255, 255), (255, 0, 255), \
		(127, 127, 127), (0, 0, 255), (0, 255, 255), (127, 127, 63), (255, 0, 0), (127, 127, 0)]
	newImage = np.zeros((*x.shape, 3), dtype=np.uint8)
	for i in range(len(colors)):
		newImage[x == i] = colors[i]
	return newImage

def fToImage(img, node, dataset):
	if dataset == "Dummy":
		return (np.repeat(img[..., 0:1], 3, axis=-1) * 255).astype(np.uint8)
	elif dataset == "Slanic":
		from neural_wrappers.readers.datasets.carla.plot_utils import default, depthToImage, opticalFlowToImage

		toImageFuncs = {
			"rgb" : default,
			"hsv" : default,
			"depth1" : depthToImage,
			"semantic1" : slanicSemantic1,
			"halftone1" : default,
			"opticalflow1": opticalFlowToImage,
			"edges1": default,
			"semanticgb1": default,
			"edges2": default,
			"normals1" : default,
			"semanticsegprop1" : slanicSemantic1,
			"depth2" : depthToImage,
		}
		return toImageFuncs[node.groundTruthKey](img)
	else:
		assert False, dataset

def plotFn(x, y, t, model, dataset:str, inNode:Node):
	outNode = model.edges[-1].getNodes()[1]
	# SingleLink vs NGC
	Key = str(model.edges[0]) if len(model.edges) == 1 else "Vote"
	y = y[Key][0]
	print("Type", type(model))
	MB = len(y)

	inImgs = x[inNode.groundTruthKey]
	gtImgs = x[outNode.groundTruthKey]
	for j in range(MB):
		inImg =  fToImage(inImgs[j], inNode, dataset)
		GTImg = fToImage(gtImgs[j], outNode, dataset)
		outImg = fToImage(y[j], outNode, dataset)
		stack = np.concatenate([inImg, GTImg, outImg], axis=1)
		# TODO: metrics
		# plt.title("Result: %2.3f" % (ix, thisPred))
		tryWriteImage(stack, "%d.png" % j)
