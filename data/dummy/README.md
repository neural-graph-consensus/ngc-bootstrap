## Dummy dataset with 4 representations

### 1. Structure of the dataset

__Nodes__: RGB, Depth, Semantic, YOLONode

The first 3 nodes can be copied from CycleConcepts directly, while the 4th node must be implemented locally.

The purpose of this dataset is to show how to:
- create new nodes (YOLONode)
- train an edge (i.e. depth2yolo)
- export pseudolabels from iteration 0
- train an edge at iteration 1

The structure of the data must be in the form of:
```
- train
  - rgb
     - 0.npz, ..., N.npz
  - depth
     - 0.npz, ..., N.npz
  - semantic
  - yolo
- validation
 - rgb
 - depth
 - semantic
 - yolo
- test
 - rgb
 - depth
 - semantic
 - yolo
```
All the npz files are GT or pseudolabels at iteration 0. Based on the structure of the graph, some representations will be updated for further iterations (semantic, depth, yolo), while others will remain in place (rgb).

Let's extract the dummy data and see that it matches our expectations:
```
$ tar -xzf data.tar.gz
$ ls train/
depth  rgb  semantic  yolo

$ ls train/*
train/depth:
0.npz   11.npz  13.npz  15.npz  17.npz  19.npz  2.npz  4.npz  6.npz  8.npz
10.npz  12.npz  14.npz  16.npz  18.npz  1.npz   3.npz  5.npz  7.npz  9.npz

train/rgb:
...
```

2. Dataset CFGs

WIP