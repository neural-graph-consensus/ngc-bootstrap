import numpy as np
from pathlib import Path

N = 20
Shape = (32, 32)
NC = {"rgb" : 3, "depth" : 1, "semantic" : 12, "yolo" : 6}
for d1 in ["train", "test", "validation"]:
    for d2 in NC.keys():
        data = np.random.random((N, Shape[0], Shape[1], NC[d2]))
        p = Path("%s/%s" % (d1, d2))
        p.mkdir(exist_ok=True, parents=True)
        for j in range(N):
            np.savez_compressed("%s/%d.npz" % (p, j), data[j])
