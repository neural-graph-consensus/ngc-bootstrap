## Slanic videos dataset

For structure reasoning, see Dummy datast README.

Export train set
```
python /scratch/nvme0n1/ngc/ngc-bootstrap/data/slanic/scripts/main_process_raw_data.py --baseDir /scratch/nvme2n1/Datasets/Slanic/train_set/raw_data/ --outputDir /scratch/nvme2n1/Datasets/Slanic/train_set/processed_data --height 240 --width 456
python /scratch/nvme0n1/ngc/ngc-bootstrap/data/slanic/scripts/main_prepare_training_data.py --videoDirectories /scratch/nvme2n1/Datasets/Slanic/train_set/processed_data/ --outputDirectory /scratch/nvme0n1/ngc/ngc-bootstrap/data/slanic/train
```

Export test set
```
python main_process_test_gt_data.py --semanticGTDir /scratch/nvme2n1/Datasets/Slanic/test_set/raw_data/semantic_segmentation/ --trainProcessedDir /scratch/nvme2n1/Datasets/Slanic/train_set/processed_data --outputDir /scratch/nvme2n1/Datasets/Slanic/test_set/processed_data/ --height 240 --width 45
mv semantic1 semantic1_original
ln -s semanticGT/ semantic1
python /scratch/nvme0n1/ngc/ngc-bootstrap/data/slanic/scripts/main_prepare_training_data.py --videoDirectories /scratch/nvme2n1/Datasets/Slanic/test_set/processed_data/ --outputDirectory /scratch/nvme0n1/ngc/ngc-bootstrap/data/slanic/validation
python /scratch/nvme0n1/ngc/ngc-bootstrap/data/slanic/scripts/main_prepare_training_data.py --videoDirectories /scratch/nvme2n1/Datasets/Slanic/test_set/processed_data/ --outputDirectory /scratch/nvme0n1/ngc/ngc-bootstrap/data/slanic/test
```

