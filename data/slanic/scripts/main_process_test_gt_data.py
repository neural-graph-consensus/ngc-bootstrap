import os
import numpy as np
from pathlib import Path
from argparse import ArgumentParser
from natsort import natsorted
from shutil import copyfile
from tqdm import trange
from media_processing_lib.image import tryReadImage, imgResize
from neural_wrappers.utilities import toCategorical

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("--semanticGTDir", required=True)
	parser.add_argument("--trainProcessedDir", required=True)
	parser.add_argument("--outputDir", required=True)
	parser.add_argument("--height", required=True, type=int)
	parser.add_argument("--width", required=True, type=int)

	args = parser.parse_args()
	args.semanticGTDir = Path(os.path.realpath(os.path.abspath(args.semanticGTDir)))
	args.trainProcessedDir = Path(os.path.realpath(os.path.abspath(args.trainProcessedDir)))
	args.outputDir = Path(os.path.realpath(os.path.abspath(args.outputDir)))

	assert args.semanticGTDir.exists()
	assert args.trainProcessedDir.exists()

	return args

def semanticFunc(semanticDir, outputDir, height, width):
	print("[Slanic-Test] Part1. Processing semantic GT data.")
	inFiles = natsorted([str(x) for x in Path(semanticDir).glob("*.npz")])
	Path(outputDir).mkdir(exist_ok=True, parents=True)
	assert len(inFiles) > 0

	Path("%s/semanticGT" % outputDir).mkdir(exist_ok=True, parents=True)
	Range = trange(len(inFiles))
	for i in Range:
		x = np.load(inFiles[i])
		x = x["map"]
		assert x.shape[-1] == 12
		x = x.astype(np.uint8)
		x = imgResize(x, interpolation="nearest", height=height, width=width, resizeLib="opencv")
		# Delete haystack from GT.
		x = np.delete(x, [3], axis=-1)
		semantic = x
		# Sanity check to ensure all pixels have exactly one class
		assert semantic.sum() == (height * width) and semantic.sum(-1).std() == 0

		if i == 0:
			Range.set_description("[Slanic-Test] semanticGT - %s" % str(semantic.shape))
		np.savez_compressed("%s/semanticGT/%d.npz" % (outputDir, i), semantic)

def getSemanticGTIds(args):
	files = [str(x) for x in Path(args.semanticGTDir).glob("*.npz")]
	fileNames = [x.split("/")[-1] for x in files]
	ids = sorted([int(x.split(".")[0].split("_")[-1][-6 : ]) for x in fileNames])
	assert len(ids) > 0
	return ids

def getPseudolabelDirs(args):
	Dirs = Path(args.trainProcessedDir).iterdir()
	Dirs = filter(lambda x : x.is_dir(), Dirs)
	Dirs = list(Dirs)
	return Dirs

def part2Func(args):
	ids = getSemanticGTIds(args)
	pseudoDirs = getPseudolabelDirs(args)
	keys = [str(x).split("/")[-1] for x in pseudoDirs]

	print("[Slanic-Test] Part2. Processing all other %s data from train set." % keys)
	for key, Dir in zip(keys, pseudoDirs):
		outDir = Path("%s/%s" % (args.outputDir, key))
		outDir.mkdir(exist_ok=True, parents=True)
		Range = trange(len(ids))
		for i in Range:
			id = ids[i]
			inPath = "%s/%d.npz" % (Dir, id)
			outPath = "%s/%d.npz" % (outDir, i)

			if i == 0:
				item = np.load(inPath)["arr_0"]
				Range.set_description("[Slanic-Test] %s - %s" % (key, item.shape))

			copyfile(inPath, outPath)
	print("[Slanic-Test] Copied pseudolabels %s from %s to %s" % (keys, args.trainProcessedDir, args.outputDir))

def main():
	args = getArgs()
	print("[Slanic-Test]\n - Semantic dir: '%s'.\n - Train dir: '%s'. \n - Output dir: '%s'.\n - Resolution: %dx%d" % \
		(args.semanticGTDir, args.trainProcessedDir, args.outputDir, args.height, args.width))

	# Two step process:
	# Step 1. We need to _convert_ all GT semantic files (from PNG labeled images). We know that these labels are a
	#  subset of the training data.
	# Step 2. We need to _copy_ all other representations from train set (including RGB)

	# Step 1.
	semanticFunc(args.semanticGTDir, args.outputDir, args.height, args.width)

	# Step 2.
	part2Func(args)

if __name__ == "__main__":
	main()
