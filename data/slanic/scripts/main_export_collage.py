import os
import sys
import numpy as np
from pathlib import Path
from tqdm import trange
from argparse import ArgumentParser
from neural_wrappers.utilities import fullPath
from media_processing_lib.image import imgResize, tryWriteImage

from neural_wrappers.readers.datasets.carla.plot_utils import default, depthToImage, opticalFlowToImage
sys.path.append(os.path.abspath(os.path.dirname(__file__)) + "/../../..")
from plot_utils import slanicSemantic1
from readers.slanic_reader import SlanicReader

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("processedDir")
    parser.add_argument("outDir")
    args = parser.parse_args()
    args.processedDir = Path(fullPath(args.processedDir))
    args.outDir = Path(fullPath(args.outDir))
    assert not args.outDir.exists()
    args.outDir.mkdir(parents=True, exist_ok=False)
    return args

def main():
    args = getArgs()
    uniqueRepresentations = sorted([str(x).split("/")[-1] for x in args.processedDir.iterdir()])
    print(uniqueRepresentations)

    toImageFuncs = {
        "rgb" : default,
        "hsv" : default,
        "depth1" : lambda x : depthToImage(np.expand_dims(x, axis=-1)),
        "semantic1_orig" : slanicSemantic1,
        "semanticGT" : slanicSemantic1,
        "halftone1" : default,
        "opticalflow1": opticalFlowToImage,
        "edges1": default,
        "semanticgb1": default,
        "edges2": default,
        "normals1" : default
    }
    for k in toImageFuncs:
        assert k in uniqueRepresentations
    N = len(list((args.processedDir / "rgb").iterdir()))
    M = int(np.ceil(np.sqrt(len(uniqueRepresentations))))
    H, W = np.load("%s/rgb/0.npz" % args.processedDir)["arr_0"].shape[0 : 2]
    stack = np.zeros((H * M, W * M, 3), dtype=np.uint8)


    perm = np.random.permutation(N)
    for i in trange(N):
        ix = perm[i]
        items = {k : np.load("%s/%s/%d.npz" % (args.processedDir, k, ix))["arr_0"] for k in toImageFuncs}
        imgs = [toImageFuncs[k](items[k]) for k in items]
 
        for r in range(M):
            for c in range(M):
                if r * M + c < len(imgs):
                    img = imgResize(imgs[r * M + c], height=H, width=W)
                    stack[r * H : (r + 1) * H, c * W : (c + 1) * W] = img
        tryWriteImage(stack, "%s/%d.png" % (args.outDir, i))

if __name__ == "__main__":
    main()