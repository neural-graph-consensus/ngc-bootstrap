import sys
import os
import numpy as np
import cv2
import scipy.io
from argparse import ArgumentParser
from pathlib import Path
from natsort import natsorted
from media_processing_lib.image import tryReadImage, imgResize
from shutil import copyfile
from tqdm import trange, tqdm
from neural_wrappers.utilities import npGetInfo

sys.path.append("/scratch/nvme0n1/others/python-halftone/")
from halftone import Halftone

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("--baseDir", required=True)
    parser.add_argument("--outputDir", required=True)
    parser.add_argument("--height", required=True, type=int)
    parser.add_argument("--width", required=True, type=int)
    args = parser.parse_args()
    args.baseDir = Path(os.path.realpath(os.path.abspath(args.baseDir)))
    args.outputDir = Path(os.path.realpath(os.path.abspath(args.outputDir)))
    return args

# GB soft Edges are stored as PNG (3 identical channels)
def edgesGBFunc(inFilePath, height, width):
    edges = scipy.io.loadmat(inFilePath)
    edges = edges["gb_CS_rescaled"]
    edges = imgResize(edges, height=height, width=width, onlyUint8=False)
    edges = np.float32(edges)
    return edges

def rgbFunc(inFilePath, height, width):
    rgb = tryReadImage(inFilePath)
    rgb = imgResize(rgb, height=height, width=width, onlyUint8=False)
    rgb = np.float32(rgb) / 255
    return rgb

def halftoneFunc(inFilePath, height, width):
    rgb = tryReadImage(inFilePath)
    rgb = imgResize(rgb, height=height, width=width, onlyUint8=False)
    # Hardcoded from another experiment.
    halftone = np.array(Halftone(rgb).make(sample=3, scale=1, percentage=91))[..., 0 : 3]
    halftone = np.float32(halftone) / 255
    return halftone

def hsvFunc(inFilePath, height, width):
    rgb = tryReadImage(inFilePath)
    rgb = imgResize(rgb, height=height, width=width, onlyUint8=False)
    hsv = cv2.cvtColor(rgb, cv2.COLOR_RGB2HSV)
    hsv = np.float32(hsv) / 255
    return hsv

def semanticFunc(inFilePath, height, width):
    semantic = np.load(inFilePath)["prediction"].astype(np.float32)
    assert semantic.shape[-1] == 12, "Expected 12 classes in raw files"
    # Keeping only 11 classes (haystack==3 is ignored)
    semantic = np.delete(semantic, [3], axis=-1)
    semantic = imgResize(semantic, height=height, width=width, onlyUint8=False, interpolation="nearest")
    return semantic

def semanticSegPropFunc(inFilePath, height, width):
    semantic = np.load(inFilePath)["map"].astype(np.float32)
    assert semantic.shape[-1] == 12, "Expected 12 classes in raw files"
    # Keeping only 11 classes (haystack==3 is ignored)
    semantic = np.delete(semantic, [3], axis=-1)
    semantic = imgResize(semantic, height=height, width=width, onlyUint8=False, interpolation="nearest")
    return semantic

def depthFunc(inFilePath, height, width):
    depth = np.load(inFilePath)["arr_0"]
    depth = np.clip(depth, 0, 1)
    depth = imgResize(depth, height=height, width=width, onlyUint8=False)
    return depth

def edgesFunc(inFilePath, height, width):
    edges = np.load(inFilePath)["arr_0"]
    edges = imgResize(edges, height=height, width=width, onlyUint8=False)
    return edges

def opticalFlowFunc(inFilePath, height, width):
    flow = np.load(inFilePath)["arr_0"]
    flow = flow[0]
    # u :: [-W : W], v :: [-H : H]
    u, v = flow
    H, W = u.shape
    u /= W
    v /= H
    u = imgResize(u, height=height, width=width, onlyUint8=False)
    v = imgResize(v, height=height, width=width, onlyUint8=False)
    flow = np.stack([u, v], axis=-1)
    return flow

def normals1Func(inFilePath, height, width):
    normals = np.load(inFilePath)["arr_0"]
    normals = np.float32(normals)
    normals = imgResize(normals, height=height, width=width, onlyUint8=False)
    return normals

def depthSfm1Func(inFilePath, height, width):
    depth = np.load(inFilePath)["arr_0"]
    # Treat nans as 0
    depth[np.isnan(depth)] = 0
    # Max depth is 400m, so anything above should be ignored (sky/too far to matter)
    depth = np.clip(depth, 0, 400)
    # Normalize to 0/1
    depth = depth / 400
    # Resize
    depth = imgResize(depth, height=height, width=width, onlyUint8=False)
    return depth

def getInFiles(baseDir, inDirs):
    K = inDirs.keys()
    inFiles = {}
    for k in K:
        dir, extension, func = inDirs[k]
        Dir = baseDir / dir
        files = Dir.glob("*.%s" % extension)
        files = [str(x) for x in files]
        files = natsorted(files)
        inFiles[k] = files
    return inFiles

def main():
    args = getArgs()
    print("[Slanic]\n - Base dir: '%s'.\n - Output dir: '%s'.\n - Resolution: %dx%d" % \
        (args.baseDir, args.outputDir, args.height, args.width))

    inDirs = {
        "rgb" : ("rgb_2k/", "png", rgbFunc),
        "hsv" : ("rgb_2k/", "png", hsvFunc),
        "halftone1" : ("rgb_2k/", "png", halftoneFunc),
        "semanticgb1" : ("semantic_segmentation_gb_2k/", "png", rgbFunc),
        "semantic1" : ("semantic_segmentation_480_hard/", "npz", semanticFunc),
        "depth1" : ("depth1_240/", "npz", depthFunc),
        "edges1" : ("edges1_225/", "npz", edgesFunc),
        "opticalflow1" : ("optical_flow1_480/", "npz", opticalFlowFunc),
        "edges2" : ("edges_gb_240/", "mat", edgesGBFunc),
        "normals1" : ("normals1_540", "npz", normals1Func),
        "semanticsegprop1" : ("semantic_segmentation_segprop_2k", "npz", semanticSegPropFunc),
        "depth2" : ("depth_sfm_540", "npz", depthSfm1Func)
    }

    inFiles = getInFiles(args.baseDir, inDirs)
    Lens = {k : len(inFiles[k]) for k in inFiles}
    print("Lens: %s" % Lens)
    assert np.std(list(Lens.values())) == 0

    # For each key in funcs (not commented)
    for key in tqdm(inDirs.keys()):
        # create the key's pseudolabel directory (where we put 0.npz, 1.npz, ..., N.npz)
        outDir = args.outputDir / key
        try:
            outDir.mkdir(exist_ok=False, parents=True)
        except Exception:
            nFiles = len([x for x in outDir.glob("*.npz")])
            if nFiles == Lens[key]:
                print("Key '%s' exists. Skipping!" % key)
                continue
            else:
                print("Key '%s' exists but only %d out of %d are present. Overwriting!" % (key, nFiles, Lens[key]))

        Range = trange(len(inFiles[key]))
        # For each raw input file of this key
        for i in Range:
            inFilePath = inFiles[key][i]
            outFilePath = outDir / ("%d.npz" % i)
            # Call the function based on the input path
            func = inDirs[key][2]
            outFile = func(inFilePath, args.height, args.width)
            # assert outFile.max() <= 1 and outFile.min() >= 0
            if i == 0:
                Range.set_description("[%s] %s" % (key, outFile.shape))
            # Store the result in the output directory as i.npz
            np.savez_compressed(outFilePath, outFile)

    # Final verification
    for key in inDirs.keys():
        Dir = args.outputDir / key
        p = [str(x) for x in Dir.glob("*.npz")]
        a = np.load(p[0])["arr_0"]
        print(key, len(p), npGetInfo(a))

if __name__ == "__main__":
    main()