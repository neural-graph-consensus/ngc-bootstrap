import os
from pathlib import Path
from argparse import ArgumentParser
from natsort import natsorted
from tqdm import tqdm, trange
from neural_wrappers.utilities import fullPath

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("--inputDirectory", required=True)
    parser.add_argument("--outputDirectory", required=True)
    args = parser.parse_args()
    args.inputDirectory = Path(fullPath(args.inputDirectory))
    args.outputDirectory = Path(fullPath(args.outputDirectory))
    return args

def sanityChecks(args):
    # Sanity checks about unique representations being the same as well as number of pseudolabels being the same
    uniqueRepresentations = sorted([str(x).split("/")[-1] for x in args.inputDirectory.iterdir()])
    Dir = args.inputDirectory
    folders = list(Dir.iterdir())
    numberPseudolabels = len(list(folders[0].glob("*.npz")))
    for folder in folders:
        dirNumber = len(list(folder.glob("*.npz")))
        assert numberPseudolabels == dirNumber, "%d vs %d for %s" % (numberPseudolabels, dirNumber, str(folder))

def makeSymbolicLinks(args):
    uniqueRepresentations = sorted([str(x).split("/")[-1] for x in args.inputDirectory.iterdir()])
    print("Found %s unique representations" % uniqueRepresentations)

    assert not args.outputDirectory.exists(), "Exists: %s" % args.outputDirectory
    args.outputDirectory.mkdir(exist_ok=True, parents=True)

    for folder in uniqueRepresentations:
        inDir = args.inputDirectory / folder
        outDir = args.outputDirectory / folder
        os.symlink(inDir, outDir)

def main():
    args = getArgs()
    sanityChecks(args)
    print("Sanity checks passed!")
    makeSymbolicLinks(args)
    print("Setup done to %s" % str(args.outputDirectory))

if __name__ == "__main__":
    main()

## Inside iter0 (and stationary for rgb)
# echo """
# ls /scratch/nvme2n1/Datasets/Slanic/processed_data/halftone/ | while read line; do
#     ln -s /scratch/nvme2n1/Datasets/Slanic/processed_data/halftone/$line halftone\_$line;
# done
# """
