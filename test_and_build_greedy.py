import numpy as np
import pandas as pd
from tqdm import trange
from functools import partial
from typing import Optional
from neural_wrappers.graph import Graph
from neural_wrappers.utilities import changeDirectory
from neural_wrappers.callbacks import Callback
from cycleconcepts.models import SingleLinkGraph
from cycleconcepts.nodes import RGB

from readers import getReader
from model import getSingleLinks, getNGC
from test import testModel

# The idea is that the modelCfg's edges represent the fully path graph (xxx2target for all xxx input nodes)
# We'll benchmark each individual single link xxx2target, then, based on the validation performance, we build the NGC
#  link by link. In the end, we'll have a result for all possible graph CFGs (N=1,...,N=n)
def testAndBuildGreedy(args, fileName:str):
	graphEdges = args.modelCfg["graph"]["edges"]
	assert len(graphEdges) >= 2, "Expected at least 2 edges for this feature. Found: %s" % graphEdges
	assert "relevantMetric" in args.modelCfg

	# Get the individual single link results
	# slResults = np.zeros(len(graphEdges), dtype=np.float32)
	# ngcResults = np.zeros(len(graphEdges), dtype=np.float32)
	direction = None
	found = False
	metricNames = None
	for i in trange(len(graphEdges), desc="Single links"):
		modelName = graphEdges[i]
		args.whichSingleLink = modelName
		model = next(getSingleLinks(args))
		model.loadWeights("%s/%s/model_best.pkl" % (args.modelCfg["modelWorkingDir"], modelName))
		assert len(model.edges) == 1
		edge = model.edges[0]

		# For the first iteration, get the metrics and the index of the relevant metric (i.e. f1score for semantic)
		if i == 0:
			edgeMetrics = edge.getMetrics()
			metricNames = tuple(map(lambda x : str(x.name), edgeMetrics))
			relevantMetricIx = metricNames.index(args.modelCfg["relevantMetric"])
			slResults = np.zeros((len(graphEdges), len(edgeMetrics)), dtype=np.float32)
			ngcResults = np.zeros((len(graphEdges), len(edgeMetrics)), dtype=np.float32)

		slResult = testModel(args, model)
		metric = edge.getMetric(args.modelCfg["relevantMetric"])
		edgeMetrics = tuple(slResult[str(edge)].values())
		edgeMetrics = np.array([x.mean() if isinstance(x, np.ndarray) else x for x in edgeMetrics])
		if direction is None:
			direction = metric.getDirection()
		assert metric.getDirection() == direction
		slResults[i] = edgeMetrics
		if edge.getNodes()[0].groundTruthKey == "rgb":
			ngcResults[0] = edgeMetrics.copy()
			assert found == False
			found = True

	# Then, do the same process for each NGC.
	assert found == True, "RGB2xxx single link was expected!"
	order = (np.argsort if direction == "min" else lambda x : np.argsort(x)[::-1])(slResults[:, relevantMetricIx])
	edgeOrder, slResults = np.array(graphEdges)[order], slResults[order]

	for i in trange(1, len(slResults), desc="NGC"):
		edges = edgeOrder[0 : i + 1]
		args.modelCfg["graph"]["edges"] = edges
		model = getNGC(args)
		ngcResult = testModel(args, model)
		ngcMetrics = tuple(ngcResult["Vote"].values())
		ngcMetrics = np.array([x.mean() if isinstance(x, np.ndarray) else x for x in ngcMetrics])
		ngcResults[i] = ngcMetrics

	rowsSL, rowsNGC = [], []
	for edge, result in zip(edgeOrder, slResults):
		rowsSL.append([str(edge), *result])
	for i, result in enumerate(ngcResults):
		rowsNGC.append(["N=%d" % (i + 1), *result])

	pd.set_option("display.max_colwidth", None)
	dfSL = pd.DataFrame(rowsSL, columns=["SL", *metricNames])
	dfNGC = pd.DataFrame(rowsNGC, columns=["NGC", *metricNames])

	Str = "Relevant metric: %s" % args.modelCfg["relevantMetric"]
	Str += "\n%s" % str(dfSL)
	Str += "\n%s" % str(dfNGC)
	print(Str)

	fileName = "%s/%s" % (args.dataCfg["testResultsDir"], fileName.split("/")[-1])
	print("[testAndBuildGreedy] Exporting to: %s" % fileName)
	f = open(fileName, "a")
	f.write(Str + "\n")
	f.close()
